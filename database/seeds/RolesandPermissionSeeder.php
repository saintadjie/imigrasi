<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesandPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

        // create permissions    
        // Master Data
        Permission::updateOrCreate(['name' => 'Melihat daftar pengguna']);
        Permission::updateOrCreate(['name' => 'Menambah data pengguna']);
        Permission::updateOrCreate(['name' => 'Mengubah data pengguna']);
        Permission::updateOrCreate(['name' => 'Mengubah data pengguna lain']);
        Permission::updateOrCreate(['name' => 'Menghapus data pengguna']);
        Permission::updateOrCreate(['name' => 'Mengembalikan data pengguna terhapus']);

        Permission::updateOrCreate(['name' => 'Melihat daftar dbdpripassword']);
        Permission::updateOrCreate(['name' => 'Menambah data dbdpripassword']);
        Permission::updateOrCreate(['name' => 'Menghapus data dbdpripassword']);
        Permission::updateOrCreate(['name' => 'Mengubah data dbdpripassword']);

        Permission::updateOrCreate(['name' => 'Melihat daftar vip']);
        Permission::updateOrCreate(['name' => 'Menambah data vip']);
        Permission::updateOrCreate(['name' => 'Menghapus data vip']);
        Permission::updateOrCreate(['name' => 'Mengubah data vip']);

        Permission::updateOrCreate(['name' => 'Melihat daftar mpaspor']);
        Permission::updateOrCreate(['name' => 'Menambah data mpaspor']);
        Permission::updateOrCreate(['name' => 'Menghapus data mpaspor']);
        Permission::updateOrCreate(['name' => 'Mengubah data mpaspor']);

        Permission::updateOrCreate(['name' => 'Melihat daftar lhi']);
        Permission::updateOrCreate(['name' => 'Menambah data lhi']);
        Permission::updateOrCreate(['name' => 'Menghapus data lhi']);
        Permission::updateOrCreate(['name' => 'Mengubah data lhi']);

        Permission::updateOrCreate(['name' => 'Melihat daftar lhi error']);
        Permission::updateOrCreate(['name' => 'Menambah data lhi error']);
        Permission::updateOrCreate(['name' => 'Menghapus data lhi error']);
        Permission::updateOrCreate(['name' => 'Mengubah data lhi error']);

        Permission::updateOrCreate(['name' => 'Melihat daftar kirka']);
        Permission::updateOrCreate(['name' => 'Menambah data kirka']);
        Permission::updateOrCreate(['name' => 'Menghapus data kirka']);
        Permission::updateOrCreate(['name' => 'Mengubah data kirka']);

        Permission::updateOrCreate(['name' => 'Melihat data dukcapil']);

        Permission::updateOrCreate(['name' => 'Melihat data simponi']);

        Permission::updateOrCreate(['name' => 'Melihat daftar synxchro']);

        Permission::updateOrCreate(['name' => 'Melihat daftar logs']);

        // create roles and assign existing permissions
        
        $role = Role::updateOrCreate(['name' => 'SUPERADMIN']);
        $role->givePermissionTo([	
                                    'Melihat daftar pengguna',
                                    'Menambah data pengguna',
                                    'Mengubah data pengguna',
                                    'Mengubah data pengguna lain',
                                    'Menghapus data pengguna',
                                    'Mengembalikan data pengguna terhapus',
                                    'Melihat daftar dbdpripassword', 
                                    'Menambah data dbdpripassword', 
                                    'Menghapus data dbdpripassword',
                                    'Mengubah data dbdpripassword',
                                    'Melihat daftar vip', 
                                    'Menambah data vip', 
                                    'Menghapus data vip',
                                    'Mengubah data vip',
                                    'Melihat daftar mpaspor', 
                                    'Menambah data mpaspor', 
                                    'Menghapus data mpaspor',
                                    'Mengubah data mpaspor',
                                    'Melihat daftar lhi', 
                                    'Menambah data lhi', 
                                    'Menghapus data lhi',
                                    'Mengubah data lhi',
                                    'Melihat daftar lhi error', 
                                    'Menambah data lhi error', 
                                    'Menghapus data lhi error',
                                    'Mengubah data lhi error',
                                    'Melihat daftar kirka', 
                                    'Menambah data kirka', 
                                    'Menghapus data kirka',
                                    'Mengubah data kirka',
                                    'Melihat data dukcapil',
                                    'Melihat data simponi',
                                    'Melihat daftar synxchro',
                                    'Melihat daftar logs',
        						]);

        $role = Role::updateOrCreate(['name' => 'PENGGUNA']);
        $role->givePermissionTo([   
                                    'Melihat daftar pengguna', 
                                    'Menambah data pengguna', 
                                    'Mengubah data pengguna',
                                    'Mengubah data pengguna lain',
                                    'Menghapus data pengguna',
                                    'Mengembalikan data pengguna terhapus',
                                ]);

       	$role = Role::updateOrCreate(['name' => 'DBDPRI']);
        $role->givePermissionTo([	
                                    'Melihat daftar dbdpripassword', 
                                    'Menambah data dbdpripassword', 
                                    'Menghapus data dbdpripassword',
                                    'Mengubah data dbdpripassword',
        						]);

        $role = Role::updateOrCreate(['name' => 'APAPO']);
        $role->givePermissionTo([   
                                    'Melihat daftar vip', 
                                    'Menambah data vip', 
                                    'Menghapus data vip',
                                    'Mengubah data vip',
                                ]);

        $role = Role::updateOrCreate(['name' => 'MPASPOR']);
        $role->givePermissionTo([   
                                    'Melihat daftar mpaspor', 
                                    'Menambah data mpaspor', 
                                    'Menghapus data mpaspor',
                                    'Mengubah data mpaspor',
                                ]);

        $role = Role::updateOrCreate(['name' => 'LHI']);
        $role->givePermissionTo([   
                                    'Melihat daftar lhi', 
                                    'Menambah data lhi', 
                                    'Menghapus data lhi',
                                    'Mengubah data lhi',
                                    'Melihat daftar lhi error', 
                                    'Menambah data lhi error', 
                                    'Menghapus data lhi error',
                                    'Mengubah data lhi error',
                                    'Melihat daftar kirka', 
                                    'Menambah data kirka', 
                                    'Menghapus data kirka',
                                    'Mengubah data kirka',
                                ]);

        $role = Role::updateOrCreate(['name' => 'DUKCAPIL']);
        $role->givePermissionTo([   
                                    'Melihat data dukcapil',
                                ]);

        $role = Role::updateOrCreate(['name' => 'SIMPONI']);
        $role->givePermissionTo([   
                                    'Melihat data simponi',
                                ]);

        $role = Role::updateOrCreate(['name' => 'SYNXCHRO']);
        $role->givePermissionTo([   
                                    'Melihat daftar synxchro',
                                ]);

        $role = Role::updateOrCreate(['name' => 'LOGS']);
        $role->givePermissionTo([   
                                    'Melihat daftar logs',
                                ]);
    }


}
