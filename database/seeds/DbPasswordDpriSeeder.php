<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;
use App\Models\DbPasswordDpri;

class DbPasswordDpriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file = storage_path() . '/app/seeder/administratif.xlsx';
        $reader = ReaderFactory::create(Type::XLSX);
        
        $reader->open($file);

        foreach ($reader->getSheetIterator() as $sheet) {

            if ($sheet->getName() === 'DBDPRI') {

                foreach ($sheet->getRowIterator() as $key => $row) {
                    if ($key > 1) {

                        $data = [
                            'name' => $row[0],
                            'dpri_app_ip' => $row[1],
                            'dpri_app_sv_password' => $row[2],
                            'dpri_db_user' => $row[3],
                            'dpri_db_password' => $row[4],
                            'dpri_db_port' => $row[5],
                            'status' => $row[6],
                        ];

                        $this->fillData(new DbPasswordDpri(), $data);
                    }
                }

            }

            

        } 

        $reader->close();

    }

    function fillData($model, $data) 
    {
        $model->fill($data);
        $model->save();
    }
}
