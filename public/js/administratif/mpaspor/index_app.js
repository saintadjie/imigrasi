$(function () {

    $(document).on('click', '#btn_simpan', function() {
        var id              = $(this).closest('tr').attr('id')
        var is_delete       = $(this).closest('tr').find('#is_delete').val()
        var is_hold         = $(this).closest('tr').find('#is_hold').val()

        Swal.fire({
            title: "Apakah anda yakin?",
            text: "Detail Permohonan akan diubah?",
            icon: "warning",
            showCancelButton: true,
            confirmButtonColor: "#3051d3",
            cancelButtonColor: '#d33',
            cancelButtonText: "Batal"
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    type: "POST",
                    url: url_api_update_detail,
                    data: {
                        id              : id,
                        is_delete       : is_delete,
                        is_hold         : is_hold,
                    },
                    success: function(data) {
                        if(data.status == 'OK'){
                            Swal.fire({
                                icon: "success",
                                title: "Data telah diubah",
                                timer: 3000,
                                html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                showClass: {
                                    popup: 'animate__animated animate__jackInTheBox'
                                },
                                hideClass: {
                                    popup: 'animate__animated animate__zoomOut'
                                },
                                backdrop: `
                                    rgba(0,0,123,0.4)
                                    url("../images/nyan-cat.gif")
                                    left top
                                    no-repeat
                                  `,
                                timerProgressBar: true,
                                onBeforeOpen: () => {
                                    Swal.showLoading()
                                    timerInterval = setInterval(() => {
                                      const content = Swal.getContent()
                                      if (content) {
                                        const b = content.querySelector('b')
                                        if (b) {
                                          b.textContent = Swal.getTimerLeft()
                                        }
                                      }
                                    }, 100)
                                },
                                onClose: () => {
                                    clearInterval(timerInterval)
                                }
                            }).then(function(){
                                location.reload();
                            });
                        } else {
                            Swal.fire({
                                icon: 'error',
                                allowOutsideClick : false,
                                title: 'Kesalahan',
                                html: 'Data gagal diubah',
                                showConfirmButton: true,
                                showClass: {
                                    popup: 'animate__animated animate__wobble'
                                },
                                hideClass: {
                                    popup: 'animate__animated animate__fadeOutRightBig'
                                },
                                backdrop: `
                                    rgba(0,0,123,0.4)
                                    url("../images/nyan-cat.gif")
                                    left top
                                    no-repeat
                                  `,
                            })
                        }   
                    }
                });
            }
        })
    });

    $('#btn_kpnkb').click(function(){

        if ($('#kpnkb').val() == '') {
            Swal.fire( "Kesalahan", "Kolom tidak boleh kosong", "error" )
            return
        }

        $.ajax({
            type: "POST",
            url: url_api_kpnkb,
            data: {
                kpnkb    : $("#kpnkb").val(),
            },
            beforeSend: function() {
                var file_loader = Math.floor(Math.random() * file_name_loader.length);
                swal.fire({
                    html: '<h5 style="color: white; text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000;">Mohon ditunggu...</h5>',
                    background: '#fff url(../images/'+file_name_loader[file_loader]+')',
                    showConfirmButton: false,
                    allowOutsideClick : false,
                    onRender: function() {
                         // there will only ever be one sweet alert open.
                         $('.swal2-content').prepend(sweet_loader);
                    }
                });
            },
            success: function(data) {

                $nomor = 0;
                if(data.status == 'OK'){
                    var html = '<div class="table-responsive"><table id="tb_imigrasi" class="table table-striped table-hover table-bordered table-sm" style="border-collapse: collapse; border-spacing: 0; width: 100%;">'+
                    '<thead bgcolor="DarkSlateBlue"><tr>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">No.</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">ID</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">ID Master</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Kode Permohonan</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">NIK</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Nama</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Satuan Kerja</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Status Detail</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Status Permohonan</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Kode Billing</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Kadaluarsa Kode Billing</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">NTB</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">NTPN</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Tanggal Submit</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Tanggal Kedatangan</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Sesi</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Ready to Sync</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Compiled At</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Is_Delete</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Is_Hold</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Rescheduled?</th>'+
                    '<th style="vertical-align: middle; text-align: center; color: white;">Aksi</th></tr></thead><tbody>';
                    for(var i=0 ; i<data.rs.length ; i++) {
                        $nomor++;
                        html = html + '<tr id="' + data.rs[i].id + '"><td>' + $nomor + '</td>'+
                        '<td>' + data.rs[i].id + '</td>'+
                        '<td>' + data.rs[i].id_master + '</td>'+
                        '<td>' + data.rs[i].kode_permohonan + '</td>'+
                        '<td>' + data.rs[i].nik + '</td>'+
                        '<td>' + data.rs[i].nama + '</td>'+
                        '<td>' + data.rs[i].satker + '</td>'+
                        '<td>' + data.rs[i].status_detail + '</td>'+
                        '<td>' + data.rs[i].status_permohonan + '</td>'+
                        '<td>' + data.rs[i].simponi_billing_code + '</td>'+
                        '<td>' + data.rs[i].kadalauarsa_kode_billing + '</td>'+
                        '<td>' + data.rs[i].simponi_ntb + '</td>'+
                        '<td>' + data.rs[i].simponi_ntpn + '</td>'+
                        '<td>' + data.rs[i].tanggal_submit + '</td>'+
                        '<td>' + data.rs[i].tanggal_kedatangan + '</td>'+
                        '<td>' + data.rs[i].nama_sesi + '</td>'+
                        '<td>' + data.rs[i].ready_to_sync + '</td>'+
                        '<td>' + data.rs[i].compiled_at + '</td>'+
                        '<td>'+ '<input type="number" min="0" max="4" id="is_delete" name="is_delete_' + i + '" class="form-control" value="' + data.rs[i].is_delete + '">' + '</td>' +
                        '<td>'+ '<input type="number" min="0" max="4" id="is_hold" name="is_hold_' + i + '" class="form-control" value="' + data.rs[i].is_hold + '">' + '</td>' +
                        '<td>' + data.rs[i].rescheduled + '</td>'+
                        '<td>' + '<button type="button" class="btn btn-outline-danger btn-rounded btn-sm waves-effect waves-light fas fa-recycle" id="btn_simpan">Update</button>' + '</td></tr>';
                    }
                    html = html + '</tbody></table></div>';
                    Swal.fire({
                        title: "Data ditemukan!",
                        // imageUrl: ("/images/komang.jpeg"),
                        // imageWidth: 100,
                        // imageHeight: 100,
                        // imageAlt: 'Sukses!',
                        icon: 'success',
                        showClass: {
                            popup: 'animate__animated animate__jackInTheBox'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__zoomOut'
                        },
                        customClass: 'swal-wide',
                        allowOutsideClick : false,
                        html: html,
                        showConfirmButton: true,
                        showCloseButton: true,
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("../images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    }).then(function (result) {
                        $('#kpnkb').val('')
                    })
                } else {
                    Swal.fire({
                        title: "Kesalahan",
                        html: "Kode Permohonan, NIK atau Kode Billing tidak ditemukan!",
                        icon: 'error',
                        showCloseButton: true,
                        showClass: {
                            popup: 'animate__animated animate__wobble'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutRightBig'
                        },
                    })
                    $('#kpnkb').val('')
                }
            }
        })
    });


    $('#btn_reset').click(function(){
        if ($('#kode_permohonan_reset').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Kode Permohonan tidak boleh kosong", "error" )
            return
        }

        $.ajax({
            type: "POST",
            url: url_api_reset,
            data: {
                kode_permohonan_reset    : $("#kode_permohonan_reset").val(),
            },
            beforeSend: function() {
                var file_loader = Math.floor(Math.random() * file_name_loader.length);
                swal.fire({
                    html: '<h5 style="color: white; text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000;">Mohon ditunggu...</h5>',
                    background: '#fff url(../images/'+file_name_loader[file_loader]+')',
                    showConfirmButton: false,
                    allowOutsideClick : false,
                    onRender: function() {
                         // there will only ever be one sweet alert open.
                         $('.swal2-content').prepend(sweet_loader);
                    }
                });
            },
            success: function(data) {
                if(data.status == 'OK'){
                    $message = data.rsmessage;
                    $id = data.rsid;
                    $nama = data.rsnama;
                    $kedatangan = data.rskedatangan;
                    Swal.fire({
                        icon: 'success',
                        width: 600,
                        showClass: {
                            popup: 'animate__animated animate__jackInTheBox'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__zoomOut'
                        },
                        allowOutsideClick : false,
                        title: ''+ $message,
                        showConfirmButton: true,
                        showCloseButton: true,
                        html: '<b>ID</b> : ' + $id + '<br><b>Nama</b> : '+ $nama + '<br><b>Kedatangan</b> : '+ $kedatangan,
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("../images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    }).then(function (result) {
                        $('#kode_permohonan_reset').val('')
                    })
                } else {
                    $message = data.rsmessage;
                    Swal.fire({
                        icon: 'warning',
                        width: 600,
                        allowOutsideClick : false,
                        title: ''+ $message,
                        showConfirmButton: true,
                        showCloseButton: true,
                        showClass: {
                            popup: 'animate__animated animate__wobble'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutRightBig'
                        },
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("../images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    })
                    $('#kode_permohonan_reset').val('')
                }
            }
        })
    });

    $('#btn_check').click(function(){
        if ($('#id_check').val() == '') {
            Swal.fire( "Kesalahan", "Kolom ID Permohonan tidak boleh kosong", "error" )
            return
        }

        $.ajax({
            type: "POST",
            url: url_api_check,
            data: {
                id_check    : $("#id_check").val(),
            },
            beforeSend: function() {
                var file_loader = Math.floor(Math.random() * file_name_loader.length);
                swal.fire({
                    html: '<h5 style="color: white; text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000;">Mohon ditunggu...</h5>',
                    background: '#fff url(../images/'+file_name_loader[file_loader]+')',
                    showConfirmButton: false,
                    allowOutsideClick : false,
                    onRender: function() {
                         // there will only ever be one sweet alert open.
                         $('.swal2-content').prepend(sweet_loader);
                    }
                });
            },
            success: function(data) {
                if(data.status == 'OK'){
                    $message = data.rsmessage;
                    $rskodepermohonan = data.rskodepermohonan[0].kode_permohonan;
                    $rskedatangan = data.rskodepermohonan[0].tanggal_pengajuan;
                    $rsstatusdetail = data.rskodepermohonan[0].status_detail;
                    $rsstatusmaster = data.rskodepermohonan[0].status_permohonan;
                    Swal.fire({
                        icon: 'success',
                        width: 600,
                        showClass: {
                            popup: 'animate__animated animate__jackInTheBox'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__zoomOut'
                        },
                        allowOutsideClick : false,
                        showCloseButton: true,
                        title: ''+ $message,
                        footer: '<div class="table-responsive"><table class="table table-striped table-hover table-bordered table-sm" style="border-collapse: collapse; border-spacing: 0; width: 100%;">'+
                        '<thead><tr>'+
                        '<th style="vertical-align: middle; text-align: center;" scope="col">Kode Permohonan</th>'+
                        '<th style="vertical-align: middle; text-align: center;" scope="col">Tanggal Kedatangan</th>'+
                        '<th style="vertical-align: middle; text-align: center;" scope="col">Status Detail</th>'+
                        '<th style="vertical-align: middle; text-align: center;" scope="col">Status Master</th></tr></thead>' + 
                        '<tbody><tr><td style="vertical-align: middle; text-align: center;">' + $rskodepermohonan + '</td>'+
                        '<td style="vertical-align: middle; text-align: center;">' + $rskedatangan + '</td>'+
                        '<td style="vertical-align: middle; text-align: center;">' + $rsstatusdetail + '</td>'+
                        '<td style="vertical-align: middle; text-align: center;">' + $rsstatusmaster + '</td></tr></tbody></table></div>',
                        showConfirmButton: true,
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("../images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    }).then(function (result) {
                        $('#id_check').val('')
                    })
                } else {
                    $message = data.rsmessage;
                    Swal.fire({
                        icon: 'warning',
                        width: 600,
                        allowOutsideClick : false,
                        title: ''+ $message,
                        showConfirmButton: true,
                        showCloseButton: true,
                        showClass: {
                            popup: 'animate__animated animate__wobble'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutRightBig'
                        },
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("../images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    })
                    $('#id_check').val('')
                }
            }
        })
    });

    $('#btn_compile').click(function(){
        if ($('#kode_permohonan_compile').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Kode Permohonan tidak boleh kosong", "error" )
            return
        }

        $.ajax({
            type: "POST",
            url: url_api_compile,
            data: {
                kode_permohonan_compile    : $("#kode_permohonan_compile").val(),
            },
            beforeSend: function() {
                var file_loader = Math.floor(Math.random() * file_name_loader.length);
                swal.fire({
                    html: '<h5 style="color: white; text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000;">Mohon ditunggu...</h5>',
                    background: '#fff url(../images/'+file_name_loader[file_loader]+')',
                    showConfirmButton: false,
                    allowOutsideClick : false,
                    onRender: function() {
                         // there will only ever be one sweet alert open.
                         $('.swal2-content').prepend(sweet_loader);
                    }
                });
            },
            success: function(data) {
                if(data.status == 'OK'){
                    $message = data.rsmessage;
                    Swal.fire({
                        icon: 'success',
                        width: 600,
                        showClass: {
                            popup: 'animate__animated animate__jackInTheBox'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__zoomOut'
                        },
                        allowOutsideClick : false,
                        html: '<p class="titik">'+ $message + '</p>',
                        showConfirmButton: true,
                        showCloseButton: true,
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("../images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    }).then(function (result) {
                        $('#kode_permohonan_compile').val('')
                    })
                } else {
                    $message = data.rsmessage;
                    Swal.fire({
                        icon: 'warning',
                        width: 600,
                        allowOutsideClick : false,
                        showConfirmButton: true,
                        showCloseButton: true,
                        html: '<p class="titik">'+ $message + '</p>',
                        showClass: {
                            popup: 'animate__animated animate__wobble'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutRightBig'
                        },
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("../images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    })
                    $('#kode_permohonan_compile').val('')
                }
            }
        })
    });

    $('#btn_kadaluarsa').click(function(){
        if ($('#kode_permohonan_kadaluarsa').val() == '') {
            Swal.fire( "Kesalahan", "Kolom ID Permohonan tidak boleh kosong", "error" )
            return
        }

        $.ajax({
            type: "POST",
            url: url_api_kadaluarsa,
            data: {
                kode_permohonan_kadaluarsa    : $("#kode_permohonan_kadaluarsa").val(),
            },
            beforeSend: function() {
                var file_loader = Math.floor(Math.random() * file_name_loader.length);
                swal.fire({
                    html: '<h5 style="color: white; text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000;">Mohon ditunggu...</h5>',
                    background: '#fff url(../images/'+file_name_loader[file_loader]+')',
                    showConfirmButton: false,
                    allowOutsideClick : false,
                    onRender: function() {
                         // there will only ever be one sweet alert open.
                         $('.swal2-content').prepend(sweet_loader);
                    }
                });
            },
            success: function(data) {
                if(data.status == 'OK'){
                    Swal.fire({
                        icon: 'success',
                        width: 600,
                        showClass: {
                            popup: 'animate__animated animate__jackInTheBox'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__zoomOut'
                        },
                        allowOutsideClick : false,
                        html: 'Data berhasil diubah menjadi kadaluarsa',
                        showConfirmButton: true,
                        showCloseButton: true,
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("../images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    }).then(function (result) {
                        $('#kode_permohonan_kadaluarsa').val('')
                    })
                } else if(data.status == 'ERROR'){

                    Swal.fire({
                        icon: 'warning',
                        width: 600,
                        allowOutsideClick : false,
                        showConfirmButton: true,
                        showCloseButton: true,
                        html: 'Data gagal diubah menjadi kadaluarsa',
                        showClass: {
                            popup: 'animate__animated animate__wobble'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutRightBig'
                        },
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("../images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    })
                    $('#kode_permohonan_kadaluarsa').val('')
                } else if(data.status == 'INVALID'){

                    Swal.fire({
                        icon: 'warning',
                        width: 600,
                        allowOutsideClick : false,
                        showConfirmButton: true,
                        showCloseButton: true,
                        html: 'ID Permohonan tidak ditemukan',
                        showClass: {
                            popup: 'animate__animated animate__wobble'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutRightBig'
                        },
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("../images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    })
                    $('#kode_permohonan_kadaluarsa').val('')
                } 
            }
        })
    });

    $('#btn_reschedule').click(function(){
        if ($('#kode_permohonan_reschedule').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Kode Permohonan tidak boleh kosong", "error" )
            return
        }

        $.ajax({
            type: "POST",
            url: url_api_reschedule,
            data: {
                kode_permohonan_reschedule    : $("#kode_permohonan_reschedule").val(),
            },
            beforeSend: function() {
                var file_loader = Math.floor(Math.random() * file_name_loader.length);
                swal.fire({
                    html: '<h5 style="color: white; text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000;">Mohon ditunggu...</h5>',
                    background: '#fff url(../images/'+file_name_loader[file_loader]+')',
                    showConfirmButton: false,
                    allowOutsideClick : false,
                    onRender: function() {
                         // there will only ever be one sweet alert open.
                         $('.swal2-content').prepend(sweet_loader);
                    }
                });
            },
            success: function(data) {
                if(data.status == 'OK'){
                    Swal.fire({
                        icon: 'success',
                        width: 800,
                        showClass: {
                            popup: 'animate__animated animate__jackInTheBox'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__zoomOut'
                        },
                        allowOutsideClick : false,
                        title: 'Kode Permohonan ditemukan',
                        showConfirmButton: true,
                        showCloseButton: true,
                        html : '<div class="table-responsive">' + 
                        '<table class="table table-striped table-hover table-bordered table-sm" style="border-collapse: collapse; border-spacing: 0; width: 100%;">'+
                        '<tbody><tr>'+
                        '<th scope="row">NIK</th>' + 
                        '<td>'+ data.rs[0].nik + '</td>' +
                        '</tr><tr>' + 
                        '<th scope="row">Nama</th>' + 
                        '<td>'+ data.rs[0].nama + '</td>' +
                        '</tr><tr>' + 
                        '<th scope="row">Kode Permohonan</th>' + 
                        '<td>'+ data.rs[0].kode_permohonan + '</td>' +
                        '</tr><tr>' +
                        '<th scope="row">Jadwal Awal</th>' + 
                        '<td>'+ data.rs[0].kanim_sebelum + ', ' + data.rs[0].tanggal_sebelum + '(' + data.rs[0].sesi_sebelum + ')' + '</td>' +
                        '</tr><tr>' + 
                        '<th scope="row">Jadwal Reschedule</th>' + 
                        '<td>'+ data.rs[0].kanim_sesudah + ', ' + data.rs[0].tanggal_sesudah + '(' + data.rs[0].sesi_sesudah + ')' + '</td>' +
                        '</tr>' + 
                        '</tbody></table></div>',

                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("../images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    }).then(function (result) {
                        $('#kode_permohonan_reschedule').val('')
                    })
                } else {
                    Swal.fire({
                        icon: 'warning',
                        width: 600,
                        allowOutsideClick : false,
                        title: 'Kode Permohonan tidak ditemukan',
                        showConfirmButton: true,
                        showCloseButton: true,
                        showClass: {
                            popup: 'animate__animated animate__wobble'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutRightBig'
                        },
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("../images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    })
                    $('#kode_permohonan_reschedule').val('')
                }
            }
        })
    });

    $('#btn_status_email').click(function(){
        if ($('#status_email').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Kode Permohonan tidak boleh kosong", "error" )
            return
        }

        $.ajax({
            type: "POST",
            url: url_api_status_email,
            data: {
                status_email    : $("#status_email").val(),
            },
            beforeSend: function() {
                var file_loader = Math.floor(Math.random() * file_name_loader.length);
                swal.fire({
                    html: '<h5 style="color: white; text-shadow: -1px -1px 0 #000, 1px -1px 0 #000, -1px 1px 0 #000, 1px 1px 0 #000;">Mohon ditunggu...</h5>',
                    background: '#fff url(../images/'+file_name_loader[file_loader]+')',
                    showConfirmButton: false,
                    allowOutsideClick : false,
                    onRender: function() {
                         // there will only ever be one sweet alert open.
                         $('.swal2-content').prepend(sweet_loader);
                    }
                });
            },
            success: function(data) {
                if(data.status == 'OK'){
                    Swal.fire({
                        icon: 'success',
                        width: 800,
                        showClass: {
                            popup: 'animate__animated animate__jackInTheBox'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__zoomOut'
                        },
                        allowOutsideClick : false,
                        title: 'Email Ditemukan',
                        showConfirmButton: true,
                        showCloseButton: true,
                        confirmButtonText:'<i class="fa fa-thumbs-up"></i> Update!',
                        showCancelButton: true,
                        html : '<div class="table-responsive">' + 
                        '<table class="table table-striped table-hover table-bordered table-sm" style="border-collapse: collapse; border-spacing: 0; width: 100%;">'+
                        '<tbody><tr>'+
                        '<th scope="row">ID</th>' + 
                        '<td>'+ '<input type="text" id="email_id" class="form-control" value="' + data.rs[0].id + '">' + '</td>' +
                        '</tr><tr>' +
                        '<th scope="row">Email</th>' + 
                        '<td>'+ '<input type="text" id="email_email" class="form-control" value="' + data.rs[0].email + '">' + '</td>' +
                        '</tr><tr>' + 
                        '<th scope="row">Nomor Telepon</th>' + 
                        '<td>'+ '<input type="text" id="email_nomor_telepon" class="form-control" value="' + data.rs[0].nomor_telepon + '">' + '</td>' +
                        '</tr><tr>' + 
                        '<th scope="row">Status</th>' + 
                        '<td>'+ '<input type="text" id="email_status" class="form-control" value="' + data.rs[0].status + '">' + '</td>' +
                        '</tr><tr>' +
                        '<th scope="row">Is Delete</th>' + 
                        '<td>'+ '<input type="text" id="email_is_delete" class="form-control" value="' + data.rs[0].is_delete + '">' + '</td>' +
                        '</tr>' + 
                        '</tbody></table></div>',

                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("../images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    }).then(function (result) {
                        if (result.value) {
                            $.ajax({
                                type: "POST",
                                url: url_api_status_email_update,
                                data: {
                                    email_id            : $("#email_id").val(),
                                    email_email         : $("#email_email").val(),
                                    email_nomor_telepon : $("#email_nomor_telepon").val(),
                                    email_status        : $("#email_status").val(),
                                    email_is_delete     : $("#email_is_delete").val(),
                                },
                                success: function(data) {
                                    if(data.status == 'OK'){
                                        Swal.fire({
                                            icon: "success",
                                            title: "Data telah diubah",
                                            timer: 3000,
                                            showCloseButton: true,
                                            html: 'Otomatis tertutup dalam <b></b> milidetik.',
                                            showClass: {
                                                popup: 'animate__animated animate__jackInTheBox'
                                            },
                                            hideClass: {
                                                popup: 'animate__animated animate__zoomOut'
                                            },
                                            backdrop: `
                                                rgba(0,0,123,0.4)
                                                asset("/images/nyan-cat.gif")
                                                left top
                                                no-repeat
                                              `,
                                            timerProgressBar: true,
                                            onBeforeOpen: () => {
                                                Swal.showLoading()
                                                timerInterval = setInterval(() => {
                                                  const content = Swal.getContent()
                                                  if (content) {
                                                    const b = content.querySelector('b')
                                                    if (b) {
                                                      b.textContent = Swal.getTimerLeft()
                                                    }
                                                  }
                                                }, 100)
                                            },
                                            onClose: () => {
                                                clearInterval(timerInterval)
                                            }
                                        }).then(function(){
                                            location.reload();
                                        });
                                    } else {
                                        Swal.fire({
                                            icon: 'error',
                                            allowOutsideClick : false,
                                            title: 'Kesalahan',
                                            html: 'Data gagal diubah',
                                            showConfirmButton: true,
                                            showCloseButton: true,
                                            showClass: {
                                                popup: 'animate__animated animate__wobble'
                                            },
                                            hideClass: {
                                                popup: 'animate__animated animate__fadeOutRightBig'
                                            },
                                            backdrop: `
                                                rgba(0,0,123,0.4)
                                                asset("../images/nyan-cat.gif")
                                                left top
                                                no-repeat
                                              `,
                                        })
                                    }
                                }
                            })
                        }
                        
                    })
                } else {
                    Swal.fire({
                        icon: 'warning',
                        width: 600,
                        allowOutsideClick : false,
                        title: 'Email tidak ditemukan',
                        showConfirmButton: true,
                        showCloseButton: true,
                        showClass: {
                            popup: 'animate__animated animate__wobble'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutRightBig'
                        },
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("../images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    })
                    $('#status_email').val('')
                }
            }
        })
    });
    

})