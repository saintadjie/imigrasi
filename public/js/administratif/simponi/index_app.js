$(function () {

    $('#btn_check').click(function(){
        if ($('#kode_billing').val() == '') {
            Swal.fire( "Kesalahan", "Kolom Kode Billing tidak boleh kosong", "error" )
            return
        }

        $.ajax({
            type: "POST",
            url: url_api_check,
            data: {
                kode_billing         : $("#kode_billing").val(),
            },
            success: function(data) {
                if(data.status == 'OK'){
                    $kode_billing        = data.result.data[3];
                    var html = '<div class="table-responsive"><table class="table table-striped table-hover table-bordered table-sm" style="border-collapse: collapse; border-spacing: 0; width: 100%;">'+
                    '<thead><tr>'+
                    '<th scope="col">Kode Billing</th>'+
                    '<th scope="col">NTB</th>'+
                    '<th scope="col">NTPN</th>'+
                    '<th scope="col">Tgl Jam Pembayaran</th>'+
                    '<th scope="col">Tgl Buku</th>'+
                    '<th scope="col">Bank Persepsi</th>'+
                    '<th scope="col">Channel Pembayaran</th>'+
                    '<th scope="col">Nama di M-Paspor</th></tr></thead>';
                   
                        html = html + '<td>' + $kode_billing + '</td>'+
                        '<td>' + data.result.response.data[1] + '</td>'+
                        '<td>' + data.result.response.data[2] + '</td>'+
                        '<td>' + data.result.response.data[3] + '</td>'+
                        '<td>' + data.result.response.data[6] + '</td>'+
                        '<td>' + data.result.response.data[4] + '</td>'+
                        '<td>' + data.result.response.data[5] + '</td>'+
                        '<td>' + data.datampaspor + '</td></tr>';

                    html = html + '</table></div>';
                    Swal.fire({
                        icon: 'success',
                        showClass: {
                            popup: 'animate__animated animate__jackInTheBox'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__zoomOut'
                        },
                        customClass: 'swal-wide',
                        html: html,
                        allowOutsideClick : false,
                        title: 'Data ditemukan',
                        showConfirmButton: true,
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("../images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    }).then(function (result) {
                        $('#kode_billing').val('')
                    })
                } else {
                    $message = data.rsmessage;
                    Swal.fire({
                        icon: 'warning',
                        width: 600,
                        allowOutsideClick : false,
                        title: ''+ $message,
                        showConfirmButton: true,
                        showClass: {
                            popup: 'animate__animated animate__wobble'
                        },
                        hideClass: {
                            popup: 'animate__animated animate__fadeOutRightBig'
                        },
                        backdrop: `
                            rgba(0,0,123,0.4)
                            url("../images/nyan-cat.gif")
                            left top
                            no-repeat
                          `,
                    })
                    $('#kode_billing').val('')
                }
            }
        })
    });

})