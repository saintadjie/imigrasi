<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DbPasswordDpri extends Model
{
    use SoftDeletes;
    protected $table = 'db_password_dpri';

    protected $fillable = [
        'name',
        'dpri_app_ip',
        'dpri_app_sv_password',
        'dpri_db_user',
        'dpri_db_password',
        'dpri_db_port',
        'status',
    ];
}
