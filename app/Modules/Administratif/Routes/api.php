<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1/administratif', 'middleware' => 'auth:api'], function () {

    Route::group(['prefix' => 'user'], function () {
        Route::DELETE('delete/{id}', 'PenggunaController@delete');
        Route::PUT('edit', 'PenggunaController@edit');
        Route::POST('search', 'PenggunaController@search');
        Route::POST('store', 'PenggunaController@store');
        Route::PUT('edit_profile', 'PenggunaController@edit_profile');
        Route::PUT('edit_password', 'PenggunaController@edit_password');
        Route::PUT('edit_password_profile', 'PenggunaController@edit_password_profile');
    });

    Route::group(['prefix' => 'apapo'], function () {
        Route::DELETE('delete/{id_vip}', 'ApapoController@delete');
        Route::PUT('edit', 'ApapoController@edit');
        Route::POST('search', 'ApapoController@search');
        Route::POST('store', 'ApapoController@store');
        Route::POST('store_vip', 'ApapoController@store_vip');
    });

    Route::group(['prefix' => 'mpaspor'], function () {
        Route::POST('get_permohonan', 'MpasporController@get_permohonan');
        Route::POST('update_detail', 'MpasporController@update_detail');
        Route::POST('refresh', 'MpasporController@refresh');
        Route::POST('reset', 'MpasporController@reset');
        Route::POST('check', 'MpasporController@check');
        Route::POST('compile', 'MpasporController@compile');
        Route::POST('kadaluarsa', 'MpasporController@kadaluarsa');
        Route::POST('check_reschedule', 'MpasporController@check_reschedule');
        Route::POST('generate_yes', 'MpasporGenerateController@generate_yes');
        Route::POST('generate_no', 'MpasporGenerateController@generate_no');
        Route::POST('generate_kadaluarsa', 'MpasporGenerateController@generate_kadaluarsa');
        Route::POST('status_email', 'MpasporController@status_email');
        Route::POST('status_email_update', 'MpasporController@status_email_update');
        Route::POST('status_permohonan_update', 'MpasporStatusController@update_status');
    });

    Route::group(['prefix' => 'dukcapil'], function () {
        Route::POST('check', 'DukcapilController@check');
    });

    Route::group(['prefix' => 'simponi'], function () {
        Route::POST('check', 'SimponiController@check');
    });

});