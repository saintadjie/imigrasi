
@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datatables/dataTables.bootstrap4.min.css')}}" />
@endpush

@section('content')
<div class="section-header">
    <h1>User</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">Users</div>
        <div class="breadcrumb-item active"><a href="#">Daftar Users</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                            <a href="{{url('administratif/user/add')}}" class="btn btn-sm btn-info">Tambah</a>
                    </div>
                </div>
                <div class="card-body">

                    <div class="table-responsive">
                        <table id="table_form" class="table table-striped table-hover" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead bgcolor="DarkSlateBlue">
                                <tr>
                                    <th style="vertical-align: middle; text-align: center; color: white;">No.</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">NIP</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Nama</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Status</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>                    
                    </div>
                </div>
            </div>
        </div>         
    </div>
</div>
    


@push('script-footer')
    <script src="{{url('out/css/datatables/datatables.min.js')}}"></script>
    <script src="{{url('out/css/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{url('js/administratif/user/index_app.js')}}"></script>
    <script src="https://cdn.datatables.net/plug-ins/1.12.0/dataRender/ellipsis.js"></script>
    <script type="text/javascript">
        var url_main    = "{{url('/administratif/user/')}}"
    </script>
    <script>
        var table_form = $('#table_form').DataTable({
            processing: true,
                serverSide: true,
            "language": {
                "emptyTable":     "Tidak ada data yang tersedia",
                "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                "infoFiltered":   "(tersaring dari _MAX_ total data)",
                "lengthMenu":     "Tampilkan _MENU_ data",
                "search":         "Pencarian:",
                "zeroRecords":    "Pencarian tidak ditemukan",
                "paginate": {
                    "first":      "Awal",
                    "last":       "Akhir",
                    "next":       "▶",
                    "previous":   "◀"
                },
            },
            "lengthMenu"  : [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
            ajax: "{{ route ('pullData.pengguna') }}",
            "columnDefs": [
                { "searchable": false, "targets": 3 }
            ],
            columns: [
                {   
                    "data": 'DT_RowIndex',
                    "sClass": "text-center",
                    "orderable": false, 
                    "searchable": false
                },
                {
                    "data": "username",
                    "sClass": "text-center",
                },
                {
                    "data": "nama",
                },
                {
                    "data": "is_status",
                    "sClass": "text-center",
                },
                {
                    "data": "id",
                    "sClass": "text-center",
                    "orderable": false,
                    "searchable": false,
                    "mRender": function (data, type, row) {

                        return '@if (auth()->user()->can('Mengubah data pengguna lain')) <a href="{{url('/administratif/user/show')}}/'+data+' " id="btn_edit" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i> Detail</a> @endif @if (auth()->user()->cannot('Mengubah data pengguna lain')) Tidak memiliki akses @endif';
                    }
                },
            ],
            "createdRow": function( row, data, dataIndex ) {
                $(row).attr('id', data.id);
            }
        });
    </script>
    
@endpush
@endsection