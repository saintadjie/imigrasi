@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('out/css/select2/select2.min.css')}}" />
@endpush

@section('content')
<div class="section-header">
    <h1>User</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">User</div>
        <div class="breadcrumb-item active"><a href="#">Tambah User</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('administratif/user')}}" class="btn btn-sm btn-danger">Kembali</a>
                    </div>
                </div>
                <div class="card-body">
                    <form id="form_imigrasi">
                        <div class="container mt-5">
                            <div class="row">
                                <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
                                    <div class="card card-primary">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="form-group col-6">
                                                    <label for="frist_name">NIP</label>
                                                    <input id="username" type="text" class="form-control" name="username" autofocus>
                                                </div>
                                                <div class="form-group col-6">
                                                    <label for="nama">Nama</label>
                                                    <input id="nama" type="text" class="form-control" name="nama">
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-6">
                                                    <label for="password">Password</label>
                                                    <input id="password" type="password" class="form-control" name="password">
                                                </div>
                                                <div class="form-group col-6">
                                                    <label for="konfirmasi_password">Konfirmasi Password</label>
                                                    <input id="konfirmasi_password" type="password" class="form-control" name="konfirmasi_password">
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label for="password">Alamat</label>
                                                <textarea id="alamat" type="text" class="form-control" name="alamat"></textarea>
                                            </div>

                                            <div class="form-group">
                                                <label>Permission</label>
                                                <select id="rspermission" name="rspermission" class="form-control select2" multiple="">
                                                    @foreach($rspermission as $rs)
                                                        <option value="{{$rs->name}}" data-idname="{{$rs->id}}">{{$rs->display}}</option>
                                                    @endforeach
                                                </select>

                                            </div>
                                
                                            <div class="form-group">
                                                <a class="btn btn-primary btn-lg btn-block text-white mr-1" id="btn_simpan">Simpan</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>         
    </div>

@push('script-footer')
    <script src="{{url('js/administratif/user/add_app.js')}}"></script>
    <script src="{{url('out/css/select2/select2.full.min.js')}}"></script>

    <script type="text/javascript">
        var url_api = "{{url('api/v1/administratif/user/store')}}"
    </script>
@endpush
@endsection