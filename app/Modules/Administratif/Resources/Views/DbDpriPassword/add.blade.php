@extends('layouts.in')
@push('script-header')
    
@endpush

@section('content')
<div class="section-header">
    <h1>Konfigurasi Administratif - Tarif</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">Konfigurasi Administratif</div>
        <div class="breadcrumb-item">Tarif</div>
        <div class="breadcrumb-item active"><a href="#">Tambah Tarif</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('administratif/tarif')}}" class="btn btn-sm btn-danger">Kembali</a>
                    </div>
                </div>
                <div class="card-body">
                    <form id="form_tarif">
                        <div class="form-row" hidden>
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="id_tarif">Id Tarif</label>
                                <input type="text" id="id_tarif" class="form-control" readonly>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="daya">Daya</label>
                                <div class="input-group">
                                    <input type="number" id="daya" class="form-control" aria-describedby="dayaHelpBlock">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            VA
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="tarifperkwh">Tarif Per KWH</label>
                                <div class="input-group">
                                    <div class="input-group-append">
                                        <div class="input-group-text">
                                            Rp.
                                        </div>
                                    </div>
                                    <input type="number" id="tarifperkwh" class="form-control" aria-describedby="tarifperkwhHelpBlock">                                    
                                </div>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 text-right">
                                <a class="btn btn-primary text-white mr-1" id="btn_simpan">Simpan</a>
                                <a class="btn btn-secondary text-white" id="btn_reset">Reset</a>
                            </div>
                            <div class="form-group col-lg-6 col-md-6"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>         
    </div>

@push('script-footer')
    <script src="{{url('js/administratif/tarif/add_app.js')}}"></script>
    <script type="text/javascript">
        $("#daya").on('input', function(){
            $('#id_tarif').val('TRF' + $('#daya').val()); 
        });

    </script>

    <script type="text/javascript">
        var url_api = "{{url('api/v1/administratif/tarif/store')}}"
    </script>
@endpush
@endsection