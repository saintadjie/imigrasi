@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css" />
    <link rel="stylesheet" type="text/css" href="{{url('out/css/select2/select2.min.css')}}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <style type="text/css">
        .titik {
          width: 100%;
          text-overflow: ellipsis;
          overflow: hidden;
        }
        .swal-wide{
            width:100% !important;
        }

        .sweet_loader {
            width: 140px;
            height: 140px;
            margin: 0 auto;
            animation-duration: 0.5s;
            animation-timing-function: linear;
            animation-iteration-count: infinite;
            animation-name: ro;
            transform-origin: 50% 50%;
            transform: rotate(0) translate(0,0);
        }
        @keyframes ro {
            100% {
                transform: rotate(-360deg) translate(0,0);
            }
        }
    </style>
@endpush

@section('content')
<div class="section-header">
    <h1>M-Paspor - API</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">M-Paspor</div>
        <div class="breadcrumb-item active"><a href="#">API</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('home')}}" class="btn btn-sm btn-danger">Kembali</a>
                    </div>
                </div>
                
                <div class="card-body">
                    
                    <div class="list-group">
                        <a href="#" class="list-group-item list-group-item-action flex-column align-items-start active">
                            <div class="d-flex w-100 justify-content-between">
                                <h5 class="mb-1">API M-Paspor</h5>
                                <small>Be careful!</small>
                            </div>
                            <p class="mb-1">Form ini memungkinkan anda untuk melakukan reset status, check simponi dan push permohonan.</p>
                            <small>Masukkan kode permohonan atau id permohonan.</small>
                        </a>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12 col-sm-12 col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-sm-12 col-md-3">
                                    <ul class="nav nav-pills flex-column" id="apiTab" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" id="kpnkb-tab" data-toggle="tab" href="#kpnkb_tab" role="tab" aria-controls="kpnkb_tab" aria-selected="true">Check Kode Permohonan</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="reset-tab" data-toggle="tab" href="#reset_tab" role="tab" aria-controls="reset_tab" aria-selected="false">Reset Status Pembayaran</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="check-tab" data-toggle="tab" href="#check_tab" role="tab" aria-controls="check_tab" aria-selected="false">Check Pembayaran Simponi</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="compile-tab" data-toggle="tab" href="#compile_tab" role="tab" aria-controls="compile_tab" aria-selected="false">Compile Permohonan</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="kadaluarsa-tab" data-toggle="tab" href="#kadaluarsa_tab" role="tab" aria-controls="kadaluarsa_tab" aria-selected="false">Kadaluarsa Permohonan</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="reschedule-tab" data-toggle="tab" href="#reschedule_tab" role="tab" aria-controls="reschedule_tab" aria-selected="false">Check Permohonan Reschedule</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" id="lain-tab" data-toggle="tab" href="#lain_tab" role="tab" aria-controls="lain_tab" aria-selected="false">Lain-Lain</a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-12 col-sm-12 col-md-9">
                                    <div class="tab-content no-padding" id="myTab2Content">
                                        <div class="tab-pane fade show active" id="kpnkb_tab" role="tabpanel" aria-labelledby="kpnkb-tab">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="list-group">
                                                        <a href="#" class="list-group-item list-group-item-action flex-column align-items-start list-group-item-dark">
                                                            <div class="d-flex w-100 justify-content-between">
                                                                <h5 class="mb-1">Check Kode Permohonan</h5>
                                                            </div>
                                                            <p class="mb-1">Masukkan Kode Permohonan, NIK atau Kode Billing Permohonan yang akan dicek</p>
                                                        </a>
                                                        <a class="list-group-item list-group-item-action flex-column align-items-start">
                                                            <div class="w-100 justify-content-between">
                                                                <div class="row">
                                                                    <div class="col-12 col-md-12 col-lg-12">
                                                                        <div class="form-group">
                                                                            <label>Kode Permohonan, NIK atau Kode Billing</label>
                                                                            <input type="number" id="kpnkb" class="form-control" aria-describedby="kpnkbHelpBlock">
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12 col-md-12 col-lg-12">
                                                                    <button id="btn_kpnkb" class="btn btn-dark btn-rounded btn-lg waves-effect waves-light" style="float: right;">Check</button>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="reset_tab" role="tabpanel" aria-labelledby="reset-tab">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="list-group">
                                                        <a href="#" class="list-group-item list-group-item-action flex-column align-items-start list-group-item-dark">
                                                            <div class="d-flex w-100 justify-content-between">
                                                                <h5 class="mb-1">Reset Status Pembayaran</h5>
                                                            </div>
                                                            <p class="mb-1">Masukkan kode permohonan yang akan direset status pembayarannya</p>
                                                        </a>
                                                        <a class="list-group-item list-group-item-action flex-column align-items-start">
                                                            <div class="w-100 justify-content-between">
                                                                <div class="row">
                                                                    <div class="col-12 col-md-12 col-lg-12">
                                                                        <div class="form-group">
                                                                            <label>Kode Permohonan</label>
                                                                            <input type="number" id="kode_permohonan_reset" class="form-control" aria-describedby="kode_permohonan_resetHelpBlock">
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12 col-md-12 col-lg-12">
                                                                    <button id="btn_reset" class="btn btn-dark btn-rounded btn-lg waves-effect waves-light" style="float: right;">Reset</button>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="check_tab" role="tabpanel" aria-labelledby="check-tab">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="list-group">
                                                        <a href="#" class="list-group-item list-group-item-action flex-column align-items-start list-group-item-dark">
                                                            <div class="d-flex w-100 justify-content-between">
                                                                <h5 class="mb-1">Check Pembayaran Simponi</h5>
                                                            </div>
                                                            <p class="mb-1">Masukkan ID permohonan yang akan dilakukan pengecekan (ID Permohonan bisa didapatkan di TAB Check Kode Permohonan)</p>
                                                        </a>
                                                        <a class="list-group-item list-group-item-action flex-column align-items-start">
                                                            <div class="w-100 justify-content-between">
                                                                <div class="row">
                                                                    <div class="col-12 col-md-12 col-lg-12">
                                                                        <div class="form-group">
                                                                            <label>ID Permohonan</label>
                                                                            <input type="number" id="id_check" class="form-control" aria-describedby="id_checkHelpBlock">
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12 col-md-12 col-lg-12">
                                                                    <button id="btn_check" class="btn btn-dark btn-rounded btn-lg waves-effect waves-light" style="float: right;">Check</button>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="compile_tab" role="tabpanel" aria-labelledby="compile-tab">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="list-group">
                                                        <a href="#" class="list-group-item list-group-item-action flex-column align-items-start list-group-item-dark">
                                                            <div class="d-flex w-100 justify-content-between">
                                                                <h5 class="mb-1">Compile Permohonan</h5>
                                                            </div>
                                                            <p class="mb-1">Masukkan kode permohonan yang akan dicompile</p>
                                                        </a>
                                                        <a class="list-group-item list-group-item-action flex-column align-items-start">
                                                            <div class="w-100 justify-content-between">
                                                                <div class="row">
                                                                    <div class="col-12 col-md-12 col-lg-12">
                                                                        <div class="form-group">
                                                                            <label>Kode Permohonan</label>
                                                                            <input type="number" id="kode_permohonan_compile" class="form-control" aria-describedby="kode_permohonan_compileHelpBlock">
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12 col-md-12 col-lg-12">
                                                                    <button id="btn_compile" class="btn btn-dark btn-rounded btn-lg waves-effect waves-light" style="float: right;">Compile</button>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="kadaluarsa_tab" role="tabpanel" aria-labelledby="kadaluarsa-tab">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="list-group">
                                                        <a href="#" class="list-group-item list-group-item-action flex-column align-items-start list-group-item-dark">
                                                            <div class="d-flex w-100 justify-content-between">
                                                                <h5 class="mb-1">Kadaluarsa Permohonan</h5>
                                                            </div>
                                                            <p class="mb-1">Masukkan Detail ID Permohonan yang akan dikadaluarsa (Detail ID Permohonan bisa didapatkan di TAB Check Kode Permohonan)</p>
                                                        </a>
                                                        <a class="list-group-item list-group-item-action flex-column align-items-start">
                                                            <div class="w-100 justify-content-between">
                                                                <div class="row">
                                                                    <div class="col-12 col-md-12 col-lg-12">
                                                                        <div class="form-group">
                                                                            <label>ID Permohonan</label>
                                                                            <input type="number" id="kode_permohonan_kadaluarsa" class="form-control" aria-describedby="kode_permohonan_kadaluarsaHelpBlock">
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12 col-md-12 col-lg-12">
                                                                    <button id="btn_kadaluarsa" class="btn btn-dark btn-rounded btn-lg waves-effect waves-light" style="float: right;">Ubah Kadaluarsa!</button>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="reschedule_tab" role="tabpanel" aria-labelledby="reschedule-tab">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="list-group">
                                                        <a href="#" class="list-group-item list-group-item-action flex-column align-items-start list-group-item-dark">
                                                            <div class="d-flex w-100 justify-content-between">
                                                                <h5 class="mb-1">Check Permohonan Reschedule</h5>
                                                            </div>
                                                            <p class="mb-1">Masukkan kode permohonan yang akan dicheck history reschedulenya</p>
                                                        </a>
                                                        <a class="list-group-item list-group-item-action flex-column align-items-start">
                                                            <div class="w-100 justify-content-between">
                                                                <div class="row">
                                                                    <div class="col-12 col-md-12 col-lg-12">
                                                                        <div class="form-group">
                                                                            <label>Kode Permohonan</label>
                                                                            <input type="number" id="kode_permohonan_reschedule" class="form-control" aria-describedby="kode_permohonan_rescheduleHelpBlock">
                                                                        </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12 col-md-12 col-lg-12">
                                                                    <button id="btn_reschedule" class="btn btn-dark btn-rounded btn-lg waves-effect waves-light" style="float: right;">Check</button>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="lain_tab" role="tabpanel" aria-labelledby="lain-tab">
                                            <div class="card">
                                                <div class="card-body">
                                                    <div class="list-group">
                                                        <a href="#" class="list-group-item list-group-item-action flex-column align-items-start list-group-item-dark">
                                                            <div class="d-flex w-100 justify-content-between">
                                                                <h5 class="mb-1">Lain-Lain</h5>
                                                            </div>
                                                            <p class="mb-1">Menu ini digunakan untuk fix minor permasalahan M-Paspor</p>
                                                        </a>
                                                    </div>
                                                    <div class="card">
                                                        <div class="card-header">
                                                            <h4>Email</h4>
                                                        </div>
                                                        <div class="card-body">
                                                            <div class="section-title mt-0">Check Status Email</div>
                                                                <div class="form-group">
                                                                    <label>Email</label>
                                                                <input type="email" id="status_email" class="form-control form-control-lg">
                                                            </div>
                                                            <div class="row">
                                                                <div class="col-12 col-md-12 col-lg-12">
                                                                    <button id="btn_status_email" class="btn btn-dark btn-rounded btn-lg waves-effect waves-light" style="float: right;">Check</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>         
    </div>
</div>

@push('script-footer')
    <script src="{{url('js/administratif/mpaspor/index_app.js')}}"></script>
 
    <script type="text/javascript">
        var url_main                    = "{{url('/administratif/mpaspor/')}}"
        var url_api_kpnkb               = "{{url('api/v1/administratif/mpaspor/get_permohonan')}}"
        var url_api_update_detail       = "{{url('api/v1/administratif/mpaspor/update_detail')}}"
        var url_api_waiting             = "{{url('api/v1/administratif/mpaspor/waiting')}}"
        var url_api_reset               = "{{url('api/v1/administratif/mpaspor/reset')}}"
        var url_api_check               = "{{url('api/v1/administratif/mpaspor/check')}}"
        var url_api_compile             = "{{url('api/v1/administratif/mpaspor/compile')}}"
        var url_api_kadaluarsa          = "{{url('api/v1/administratif/mpaspor/kadaluarsa')}}"
        var url_api_reschedule          = "{{url('api/v1/administratif/mpaspor/check_reschedule')}}"
        var url_api_status_email        = "{{url('api/v1/administratif/mpaspor/status_email')}}"
        var url_api_status_email_update = "{{url('api/v1/administratif/mpaspor/status_email_update')}}"
        var sweet_loader = '<div class="sweet_loader"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin: auto; background: none; display: block; shape-rendering: auto;" width="140px" height="140px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><circle cx="50" cy="50" r="0" fill="none" stroke="#014c86" stroke-width="2"><animate attributeName="r" repeatCount="indefinite" dur="1s" values="0;40" keyTimes="0;1" keySplines="0 0.2 0.8 1" calcMode="spline" begin="0s"></animate><animate attributeName="opacity" repeatCount="indefinite" dur="1s" values="1;0" keyTimes="0;1" keySplines="0.2 0 0.8 1" calcMode="spline" begin="0s"></animate></circle><circle cx="50" cy="50" r="0" fill="none" stroke="#00a950" stroke-width="2"><animate attributeName="r" repeatCount="indefinite" dur="1s" values="0;40" keyTimes="0;1" keySplines="0 0.2 0.8 1" calcMode="spline" begin="-0.5s"></animate><animate attributeName="opacity" repeatCount="indefinite" dur="1s" values="1;0" keyTimes="0;1" keySplines="0.2 0 0.8 1" calcMode="spline" begin="-0.5s"></animate></circle></svg></div>';
    </script>



@endpush
@endsection
