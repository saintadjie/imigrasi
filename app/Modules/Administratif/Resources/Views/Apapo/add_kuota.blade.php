@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css" />
    <link rel="stylesheet" type="text/css" href="{{url('out/css/select2/select2.min.css')}}" />
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datatables/dataTables.bootstrap4.min.css')}}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <link rel="stylesheet" type="text/css" href="{{url('out/css/select2/select2.min.css')}}" />
@endpush

@section('content')
<div class="section-header">
    <h1>APAPO - Kuota VIP</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">APAPO</div>
        <div class="breadcrumb-item">Kuota VIP</div>
        <div class="breadcrumb-item active"><a href="#">Tambah Kuota VIP</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                        <a href="{{url('administratif/apapo')}}" class="btn btn-sm btn-danger">Kembali</a>
                    </div>
                </div>
                <div class="card-body">
                    <form id="form_penggunaan">

                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-8 col-md-8">
                                <label for="tanggal">Tanggal</label>
                                <input type="text" id="tanggal" class="form-control datepicker" aria-describedby="tanggalHelpBlock" multiple>
                            </div>
                            <div class="form-group col-lg-2 col-md-2"></div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-8 col-md-8">
                                <label for="id_penggunaan">Satuan Kerja</label>
                                <select id="satker" name="rs" class="form-control select2">
                                    @foreach($rs as $rs)
                                        <option value="{{$rs->id}}" data-idname="{{$rs->unit_name}}">{{$rs->unit_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-lg-2 col-md-2"></div>
                        </div>

                        <div class="form-row">
                            <div class="form-group col-lg-2 col-md-2"></div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="quota">Kuota VIP</label>
                                <input type="number" id="quota" class="form-control" aria-describedby="quotaHelpBlock">
                            </div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="availability">Availability</label>
                                <input type="number" id="availability" class="form-control" aria-describedby="availabilityHelpBlock" multiple>
                            </div>
                            <div class="form-group col-lg-2 col-md-2"></div>
                        </div>


                        <div class="form-row">
                            <div class="form-group col-lg-10 col-md-10 text-right">
                                <a class="btn btn-primary text-white mr-1" id="btn_simpan">Simpan</a>
                            </div>
                            <div class="form-group col-lg-2 col-md-2"></div>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>         
    </div>

@push('script-footer')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>
    <script src="{{url('out/css/datatables/datatables.min.js')}}"></script>
    <script src="{{url('out/css/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{url('out/css/select2/select2.full.min.js')}}"></script>
    <script src="{{url('js/administratif/apapo/add_kuotaapp.js')}}"></script>
    <script type="text/javascript">
        var url_api         = "{{url('api/v1/administratif/apapo/store_vip')}}"
        var url_main        = "{{url('/administratif/apapo/')}}"

    </script>

    <script type="text/javascript">
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            multidate: true,
        });
    </script>
@endpush
@endsection