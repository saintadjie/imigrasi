
@extends('layouts.in')
@push('script-header')
    <link rel="stylesheet" type="text/css" href="{{url('out/css/datatables/dataTables.bootstrap4.min.css')}}" />
@endpush

@section('content')
<div class="section-header">
    <h1>APAPO - Kuota VIP</h1>
    <div class="section-header-breadcrumb">
        <div class="breadcrumb-item">APAPO</div>
        <div class="breadcrumb-item active"><a href="#">VIP</a></div>
    </div>
</div>
  
<div class="section-body">
    <div class="row">
        <div class="col-12 col-md-12 col-lg-12">
            <div class="card">
                <div class="card-header">
                    <h4></h4>
                    <div class="card-header-action">
                            <a href="{{url('administratif/apapo/add_kuota')}}" class="btn btn-sm btn-info">Buat Kuota VIP</a>
                    </div>
                </div>
                <div class="card-body">

                    <div class="table-responsive">
                        <table id="table_form" class="table table-striped table-hover" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                            <thead bgcolor="DarkSlateBlue">
                                <tr>
                                    <th style="vertical-align: middle; text-align: center; color: white;">No.</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Nama</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Kelas</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Kode</th>
                                    <th style="vertical-align: middle; text-align: center; width: 30%; color: white;">Alamat</th>
                                    <th style="vertical-align: middle; text-align: center; color: white;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                            </tbody>
                        </table>                    
                    </div>
                </div>
            </div>
        </div>         
    </div>
</div>
    


@push('script-footer')
    <script src="{{url('out/css/datatables/datatables.min.js')}}"></script>
    <script src="{{url('out/css/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{url('js/administratif/apapo/index_app.js')}}"></script>
    <script type="text/javascript">
        var url_main    = "{{url('/administratif/apapo/')}}"
    </script>
    <script>
        var table_form = $('#table_form').DataTable({
            "language": {
                "emptyTable":     "Tidak ada data yang tersedia",
                "info":           "Menampilkan _START_ hingga _END_ dari _TOTAL_ data",
                "infoEmpty":      "Menampilkan 0 hingga 0 dari 0 data",
                "infoFiltered":   "(tersaring dari _MAX_ total data)",
                "lengthMenu":     "Tampilkan _MENU_ data",
                "search":         "Pencarian:",
                "zeroRecords":    "Pencarian tidak ditemukan",
                "paginate": {
                    "first":      "Awal",
                    "last":       "Akhir",
                    "next":       "▶",
                    "previous":   "◀"
                },
            },
            "lengthMenu"  : [[10, 25, 50, -1], [10, 25, 50, "Semua"]],
            processing: true,
            serverSide: true,
            ajax: "{{ route ('pullData.vip') }}",
            columns: [
                {   
                    "data": 'DT_RowIndex',
                    "sClass": "text-center",
                    "orderable": false, 
                    "searchable": false
                },
                {
                    "data": "unit_name",
                    "sClass": "text-center",
                },
                {
                    "data": "unit_class",
                    "sClass": "text-center",
                },
                {
                    "data": "unit_code",
                    "sClass": "text-center",
                },
                {
                    "data": "office_address",
                    "sClass": "text-center",
                },
                {
                    "data": "id",
                    "sClass": "text-center",
                    "orderable": false,
                    "searchable": false,
                    "mRender": function (data, type, row) {

                        return '@if (auth()->user()->can('Mengubah data vip')) <a href="{{url('/administratif/apapo/show')}}/'+data+' " id="btn_edit" class="btn btn-icon icon-left btn-primary"><i class="far fa-edit"></i> Detail</a> @endif @if (auth()->user()->cannot('Mengubah data vip')) Tidak memiliki akses @endif';
                    }
                },
            ],
            "createdRow": function( row, data, dataIndex ) {
                $(row).attr('id', data.id);
            }
        });
    </script>
    
@endpush
@endsection