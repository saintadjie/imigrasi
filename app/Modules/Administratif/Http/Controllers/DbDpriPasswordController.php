<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\DbPasswordDpri;
use Yajra\DataTables\DataTables;
use App\User;
use Auth;

class DbDpriPasswordController extends Controller
{
    public function pullData(Request $request) {

        return datatables (DbPasswordDpri::select('name', 'dpri_app_ip', 'dpri_app_sv_password', 'dpri_db_user', 'dpri_db_password', 'dpri_db_port', 'status')
            ->where('deleted_at','=', NULL)
            ->latest())
            ->addIndexColumn()
            ->toJson();

    }

    public function index(Request $request) {


        return view('administratif::dbdpripassword.index');

    }
}
