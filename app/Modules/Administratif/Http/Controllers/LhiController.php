<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use DB;

use Carbon\Carbon;

class LhiController extends Controller
{
    protected $db;

    public function __construct() {
        $this->db = DB::connection('LHI');
    }

    public function pullData(Request $request) {
        

        try {
            $today = Carbon::now();
            $date = Carbon::now()->subDays(7)->startOfDay();
            return datatables ($this->db->select("SELECT a.ID, b.UNIT_NAME, a.ATTACHMENT_NAME, a.ATTACHMENT_SUBJECT, a.ATTACHMENT_DESCRIPTION FROM IMGPENGAWASAN.REPORT_INTEL a JOIN IMGPENGAWASAN.BUSINESS_UNIT b on a.BUSINESS_UNIT_ID = b.ID WHERE a.REPORTED_ON BETWEEN (TIMESTAMP '$date') AND (TIMESTAMP '$today')  ORDER BY a.REPORTED_ON DESC"))
            ->addIndexColumn()
            ->toJson(10);


        } catch (Exception $e) {
            report($e);
            abort(403, 'Unauthorized action.');
            return false;
        }

    }

    public function index(Request $request) {
        $today = Carbon::now()->format('d-m-Y');
        $date = Carbon::now()->subDays(7)->format('d-m-Y');

        return view('administratif::lhi.index', ['today' => $today, 'date' => $date]);

    }

    public function pullData_index_error(Request $request) {
        

        try {
            $today = Carbon::now();
            $date = Carbon::now()->subDays(7)->startOfDay();
            return datatables ($this->db->select("SELECT a.ID, b.UNIT_NAME, a.ATTACHMENT_NAME, a.ATTACHMENT_SUBJECT, a.ATTACHMENT_DESCRIPTION FROM IMGPENGAWASAN.REPORT_INTEL a JOIN IMGPENGAWASAN.BUSINESS_UNIT b on a.BUSINESS_UNIT_ID = b.ID WHERE a.CREATED_ON BETWEEN (TIMESTAMP '$date') AND (TIMESTAMP '$today') AND a.ATTACHMENT_DESCRIPTION LIKE '% %' OR a.ATTACHMENT_DESCRIPTION IS NULL OR a.ATTACHMENT_DESCRIPTION LIKE '%è%' OR a.ATTACHMENT_DESCRIPTION LIKE '%²%' OR a.ATTACHMENT_DESCRIPTION LIKE '%à%' OR a.ATTACHMENT_DESCRIPTION LIKE '%ò%' OR a.ATTACHMENT_DESCRIPTION LIKE '%±%' OR a.ATTACHMENT_DESCRIPTION LIKE '%°%' OR a.ATTACHMENT_DESCRIPTION LIKE '%´%' OR a.ATTACHMENT_DESCRIPTION LIKE '%À%' OR a.ATTACHMENT_DESCRIPTION LIKE '%ì%' OR a.ATTACHMENT_DESCRIPTION LIKE '%é%' OR a.ATTACHMENT_DESCRIPTION LIKE '%ì%' OR a.ATTACHMENT_DESCRIPTION LIKE '%¬%' OR a.ATTACHMENT_DESCRIPTION LIKE '%ñ%' OR a.ATTACHMENT_DESCRIPTION LIKE '%č%' OR a.ATTACHMENT_DESCRIPTION LIKE '%ž%' OR a.ATTACHMENT_DESCRIPTION LIKE '%á%' OR a.ATTACHMENT_DESCRIPTION LIKE '%ě%' OR a.ATTACHMENT_DESCRIPTION LIKE '%Ú%' OR a.ATTACHMENT_DESCRIPTION LIKE '%í%' OR a.ATTACHMENT_DESCRIPTION LIKE '%Č%' OR a.ATTACHMENT_DESCRIPTION LIKE '%ý%' OR a.ATTACHMENT_DESCRIPTION LIKE '%×%' OR a.ATTACHMENT_DESCRIPTION LIKE '%ö%' OR a.ATTACHMENT_DESCRIPTION LIKE '%ü%' OR a.ATTACHMENT_DESCRIPTION LIKE '%ı%' OR a.ATTACHMENT_DESCRIPTION LIKE '%ù%' OR a.ATTACHMENT_DESCRIPTION LIKE '%ç%' OR a.ATTACHMENT_DESCRIPTION LIKE '%»%' OR a.ATTACHMENT_DESCRIPTION LIKE '%£%' OR a.ATTACHMENT_DESCRIPTION LIKE '%È%' OR a.ATTACHMENT_DESCRIPTION LIKE '%Ù%' OR a.ATTACHMENT_DESCRIPTION LIKE '%⁹%' OR a.ATTACHMENT_DESCRIPTION LIKE '%­%' OR a.ATTACHMENT_DESCRIPTION LIKE '%¹%' OR a.ATTACHMENT_DESCRIPTION LIKE '%Ö%' OR a.ATTACHMENT_DESCRIPTION LIKE '%þ%' OR a.ATTACHMENT_DESCRIPTION LIKE '%⅝%' OR a.ATTACHMENT_DESCRIPTION LIKE '%ó%' OR a.ATTACHMENT_DESCRIPTION LIKE '%Ź%' OR a.ATTACHMENT_SUBJECT LIKE '% %' OR a.ATTACHMENT_SUBJECT IS NULL OR a.ATTACHMENT_SUBJECT LIKE '%è%' OR a.ATTACHMENT_SUBJECT LIKE '%²%' OR a.ATTACHMENT_SUBJECT LIKE '%à%' OR a.ATTACHMENT_SUBJECT LIKE '%ò%' OR a.ATTACHMENT_SUBJECT LIKE '%±%' OR a.ATTACHMENT_SUBJECT LIKE '%°%' OR a.ATTACHMENT_SUBJECT LIKE '%´%' OR a.ATTACHMENT_SUBJECT LIKE '%À%' OR a.ATTACHMENT_SUBJECT LIKE '%ì%' OR a.ATTACHMENT_SUBJECT LIKE '%é%' OR a.ATTACHMENT_SUBJECT LIKE '%ì%' OR a.ATTACHMENT_SUBJECT LIKE '%¬%' OR a.ATTACHMENT_SUBJECT LIKE '%ñ%' OR a.ATTACHMENT_SUBJECT LIKE '%č%' OR a.ATTACHMENT_SUBJECT LIKE '%ž%' OR a.ATTACHMENT_SUBJECT LIKE '%á%' OR a.ATTACHMENT_SUBJECT LIKE '%ě%' OR a.ATTACHMENT_SUBJECT LIKE '%Ú%' OR a.ATTACHMENT_SUBJECT LIKE '%í%' OR a.ATTACHMENT_SUBJECT LIKE '%Č%' OR a.ATTACHMENT_SUBJECT LIKE '%ý%' OR a.ATTACHMENT_SUBJECT LIKE '%×%' OR a.ATTACHMENT_SUBJECT LIKE '%ö%' OR a.ATTACHMENT_SUBJECT LIKE '%ü%' OR a.ATTACHMENT_SUBJECT LIKE '%ı%' OR a.ATTACHMENT_SUBJECT LIKE '%ù%' OR a.ATTACHMENT_SUBJECT LIKE '%ç%' OR a.ATTACHMENT_SUBJECT LIKE '%»%' OR a.ATTACHMENT_SUBJECT LIKE '%£%' OR a.ATTACHMENT_SUBJECT LIKE '%È%' OR a.ATTACHMENT_SUBJECT LIKE '%Ù%' OR a.ATTACHMENT_SUBJECT LIKE '%⁹%' OR a.ATTACHMENT_SUBJECT LIKE '%­%' OR a.ATTACHMENT_SUBJECT LIKE '%¹%' OR a.ATTACHMENT_SUBJECT LIKE '%Ö%' OR a.ATTACHMENT_SUBJECT LIKE '%þ%' OR a.ATTACHMENT_SUBJECT LIKE '%⅝%'  OR a.ATTACHMENT_SUBJECT LIKE '%ó%'  OR a.ATTACHMENT_SUBJECT LIKE '%Ź%' OR a.ATTACHMENT_NAME LIKE '% %' OR a.ATTACHMENT_NAME LIKE '%è%' OR a.ATTACHMENT_NAME LIKE '%²%' OR a.ATTACHMENT_NAME LIKE '%à%' OR a.ATTACHMENT_NAME LIKE '%ò%' OR a.ATTACHMENT_NAME LIKE '%±%' OR a.ATTACHMENT_NAME LIKE '%°%' OR a.ATTACHMENT_NAME LIKE '%´%' OR a.ATTACHMENT_NAME LIKE '%À%' OR a.ATTACHMENT_NAME LIKE '%ì%' OR a.ATTACHMENT_NAME LIKE '%é%' OR a.ATTACHMENT_NAME LIKE '%ì%' OR a.ATTACHMENT_NAME LIKE '%¬%' OR a.ATTACHMENT_NAME LIKE '%ñ%' OR a.ATTACHMENT_NAME LIKE '%č%' OR a.ATTACHMENT_NAME LIKE '%ž%' OR a.ATTACHMENT_NAME LIKE '%á%' OR a.ATTACHMENT_NAME LIKE '%ě%' OR a.ATTACHMENT_NAME LIKE '%Ú%' OR a.ATTACHMENT_NAME LIKE '%í%' OR a.ATTACHMENT_NAME LIKE '%Č%' OR a.ATTACHMENT_NAME LIKE '%ý%' OR a.ATTACHMENT_NAME LIKE '%×%' OR a.ATTACHMENT_NAME LIKE '%ö%' OR a.ATTACHMENT_NAME LIKE '%ü%' OR a.ATTACHMENT_NAME LIKE '%ı%' OR a.ATTACHMENT_NAME LIKE '%ù%' OR a.ATTACHMENT_NAME LIKE '%ç%' OR a.ATTACHMENT_NAME LIKE '%»%' OR a.ATTACHMENT_NAME LIKE '%£%' OR a.ATTACHMENT_NAME LIKE '%È%' OR a.ATTACHMENT_NAME LIKE '%Ù%' OR a.ATTACHMENT_NAME LIKE '%⁹%' OR a.ATTACHMENT_NAME LIKE '%­%' OR a.ATTACHMENT_NAME LIKE '%¹%' OR a.ATTACHMENT_NAME LIKE '%Ö%'OR a.ATTACHMENT_NAME LIKE '%þ%'OR a.ATTACHMENT_NAME LIKE '%⅝%' OR a.ATTACHMENT_NAME LIKE '%ó%' OR a.ATTACHMENT_NAME LIKE '%Ź%'"))
            ->addIndexColumn()
            ->toJson(10);


        } catch (Exception $e) {
            report($e);
            abort(403, 'Unauthorized action.');
            return false;
        }

    }

    public function index_error(Request $request) {
        
        $today = Carbon::now();
        $date = Carbon::now()->subDays(7)->startOfDay();
        
        return view('administratif::lhi.index_error',['today' => $today, 'date' => $date]);

    }
    
    public function page_show(Request $request, $id) {
        
        $id         = $request->id;

        $rs         = $this->db->select(DB::raw("SELECT a.ID as id_lhi, b.UNIT_NAME, a.ATTACHMENT_NAME, a.ATTACHMENT_SUBJECT, a.ATTACHMENT_DESCRIPTION FROM IMGPENGAWASAN.REPORT_INTEL a JOIN IMGPENGAWASAN.BUSINESS_UNIT b on a.BUSINESS_UNIT_ID = b.ID WHERE a.ID = '$id'"));
        
        // dd($rs);
        return view('administratif::lhi.show', ['rs' => $rs]);

    }

    public function page_show_error(Request $request, $id) {

        $id         = $request->id;

        $rs         = $this->db->select(DB::raw("SELECT a.ID as id_lhi, b.UNIT_NAME, a.ATTACHMENT_NAME, a.ATTACHMENT_SUBJECT, a.ATTACHMENT_DESCRIPTION FROM IMGPENGAWASAN.REPORT_INTEL a JOIN IMGPENGAWASAN.BUSINESS_UNIT b on a.BUSINESS_UNIT_ID = b.ID WHERE a.ID = '$id'"));

        $search     = array('\r\n', '\n', ' ');
        $replace    = array(',', ',', ',');
        $name       = $rs[0]->attachment_name;
        $subject    = $rs[0]->attachment_subject;
        $desc       = $rs[0]->attachment_description;
        $rsname     = str_replace($search, $replace, $name);
        $rssubject  = str_replace($search, $replace, $subject);
        $rsdesc     = str_replace($search, $replace, $desc);
        dd($rsdesc);
        dd($rs);
        $rsname     = $rs[0]->attachment_name;
        $rssubject  = $rs[0]->attachment_subject;
        $rsdesc     = $rs[0]->attachment_description;
        return view('administratif::lhi.show_error', ['rs' => $rs, 'rsname' => $rsname, 'rssubject' => $rssubject, 'rsdesc' => $rsdesc]);

    }
}
