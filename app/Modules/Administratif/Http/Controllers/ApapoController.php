<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;
use Auth;
use Illuminate\Support\Str;

class ApapoController extends Controller
{
    protected $db;

    public function __construct() {
        $this->db = DB::connection('apapo');
    }

    public function pullData(Request $request) {

        try {
            
            return datatables ($this->db->select('SELECT * FROM IMGWNI.BUSINESS_UNIT'))
            ->addIndexColumn()
            ->toJson(10);


        } catch (Exception $e) {
            report($e);
            abort(403, 'Unauthorized action.');
            return false;
        }

    }

    public function index(Request $request) {

        return view('administratif::apapo.index');

    }

    public function page_show(Request $request, $id) {

        $id         = $request->id;
        $today = Carbon::now()->subDays(1)->startOfDay();

        // $rs         = $this->db->select(DB::raw("SELECT b.ID, b.UNIT_NAME AS kantor, a.ID as id_kuota, to_char(a.SERVICE_DATE, 'dd Month yyyy','nls_date_language = INDONESIAN') as tanggal_permohonan, a.QUOTA, a.AVAILABILITY AS sisa_kuota, a.QUOTA - a.AVAILABILITY as kuota_terpakai FROM IMGWNI.UNIT_QUOTA_VIP a JOIN IMGWNI.BUSINESS_UNIT b ON a.BUSINESS_UNIT_ID = b.ID WHERE a.BUSINESS_UNIT_ID = '$id' ORDER BY service_date DESC FETCH FIRST 50 ROWS ONLY"));

        $rs         = $this->db->select(DB::raw("SELECT b.ID, b.UNIT_NAME AS kantor, a.ID as id_kuota, to_char(a.SERVICE_DATE, 'dd Month yyyy','nls_date_language = INDONESIAN') as tanggal_permohonan, a.QUOTA, a.AVAILABILITY AS sisa_kuota, a.QUOTA - a.AVAILABILITY as kuota_terpakai FROM IMGWNI.UNIT_QUOTA_VIP a JOIN IMGWNI.BUSINESS_UNIT b ON a.BUSINESS_UNIT_ID = b.ID WHERE a.BUSINESS_UNIT_ID = '$id' AND a.SERVICE_DATE >= (TIMESTAMP '$today') ORDER BY service_date DESC"));

        $countrs = count($rs);
        
        return view('administratif::apapo.show', ['rs' => $rs, 'countrs' => $countrs]);
    }

    public function store(Request $request) {

        $id             = $request->id;
        $quota          = $request->quota;
        $availability   = $request->availability;
        $username       = Auth::User()->username;

        $rs         = $this->db->update(DB::raw("UPDATE IMGWNI.UNIT_QUOTA_VIP a SET a.QUOTA = '$quota', a.AVAILABILITY = '$availability' WHERE a.ID = '$id'"));

        if ($rs){

            activity()
            ->withProperties(['username' => $username])
            ->log($username. ' mengubah Kuota VIP dengan ID '. $id);

            return response()->json(['status' => 'OK']);

        } else {

            activity()
            ->withProperties(['username' => $username])
            ->log($username. ' gagal mengubah Kuota VIP dengan ID '. $id);

            return response()->json(['status' => 'ERROR']);

        }

    }

    public function edit(Request $request) {

        $id             = $request->id;
        $quota          = $request->quota;
        $availability   = $request->availability;
        $tanggal_awal   = $request->tanggal_awal;
        $tanggal_akhir  = $request->tanggal_akhir;
        $username       = Auth::User()->username;


        $rs         = $this->db->update(DB::raw("UPDATE IMGWNI.UNIT_QUOTA_VIP a SET a.QUOTA = '$quota', a.AVAILABILITY = '$availability' WHERE a.BUSINESS_UNIT_ID = '$id' AND a.SERVICE_DATE BETWEEN TO_DATE ('$tanggal_awal', 'yyyy/mm/dd') AND TO_DATE ('$tanggal_akhir', 'yyyy/mm/dd')"));

        if ($rs){

            activity()
            ->withProperties(['username' => $username])
            ->log($username. ' mengubah Kuota VIP dengan KANIM ID '. $id .' dari tanggal '. $tanggal_awal . ' hingga '. $tanggal_akhir);

            return response()->json(['status' => 'OK']);

        } else {

            activity()
            ->withProperties(['username' => $username])
            ->log($username. ' gagal mengubah Kuota VIP dengan KANIM ID '. $id .' dari tanggal '. $tanggal_awal . ' hingga '. $tanggal_akhir);

            return response()->json(['status' => 'ERROR']);

        } 
    }

    public function page_add() {



        // $rs         = $this->db->select(DB::raw("SELECT ID, UNIT_NAME, CONCAT(CONCAT(ID, ' - '),UNIT_NAME) AS display FROM IMGWNI.BUSINESS_UNIT"));
        $rs         = $this->db->select(DB::raw("SELECT ID, UNIT_NAME FROM IMGWNI.BUSINESS_UNIT WHERE IS_ACTIVE = 1"));
        // $uuid       =  Str::uuid();
        return view('administratif::apapo.add_kuota', ['rs' => $rs]);

    }

    public function store_vip(Request $request) {
        $tanggal        = $request->tanggal;
        $tanggal        = explode(',', $tanggal);
            
        foreach($tanggal as $i =>$key) {
            $id             = Str::uuid();
            $time           = Carbon::now()->format('Y-m-d h:m:s.u');
            $satker         = $request->satker;
            $quota          = $request->quota;
            $availability   = $request->availability;
            $username       = Auth::User()->username;

            $rs         = $this->db->insert(DB::raw("INSERT INTO IMGWNI.UNIT_QUOTA_VIP (ID, CREATED_BY, CREATED_ON, MODIFIED_BY, MODIFIED_ON, APPROVED_BY, APPROVED_ON, IS_ACTIVE,IS_LOCKED, IS_DEFAULT, OWNER_ID, BUSINESS_UNIT_ID, SERVICE_DATE, START_TIME, END_TIME, QUOTA, AVAILABILITY) VALUES ('$id','$username',TIMESTAMP '$time',NULL,NULL,NULL,NULL,1,0,1,'7D72100C-100C-04AA-2FDA-E0530BDEA00A','$satker',TIMESTAMP '$key 00:00:00.000000',800,1200,'$quota','$availability')"));

        }

        if ($rs){


            return response()->json(['status' => 'OK']);

        } else {


            return response()->json(['status' => 'ERROR']);

        }

    }

}

