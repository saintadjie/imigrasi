<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Traits\HasRoles;
use App\User;
use DB;
use Hash;
use Auth;


class PenggunaController extends Controller
{
    public function pullData(Request $request) {

        try {
            return datatables (User::select('id', 'username', 'nama', 'alamat', 'status', DB::raw("(CASE WHEN status = 1 THEN 'AKTIF' ELSE 'NON AKTIF' END) AS is_status"))
                ->where('username','!=','superadmin') 
                ->latest())
                ->addIndexColumn()
                ->toJson();

            } catch (Exception $e) {
            report($e);
            abort(403, 'Unauthorized action.');
            return false;
        }
    }

    public function pullDataRoles (Request $request) {
        $type       = $request->type;

        switch ($type) {
            case 'pullDataRoles':
                if ($request->has('username')) {
                    $username       = $request->username;
                    $rs             = DB::table('roles')
                                        ->select('roles.id', 'name', DB::raw("CONCAT(name) as display"))
                                        ->join('model_has_roles', 'roles.id', 'model_has_roles.role_id')
                                        ->join('users', 'model_has_roles.model_id', 'users.id')
                                        ->where('users.username', $username)
                                        ->get();
                    return response()->json($rs);
                } 
                else if ($request->has('id')) {
                    $id             = $request->id;
                    $rs             = DB::select(DB::raw("select id, name, CONCAT(name) AS display from roles where id not in (select a.role_id from model_has_roles a where a.model_id ='$id') AND  name not in ('SUPERADMIN','PENGGUNA')"));
                    return response()->json($rs);

                }
            break;
        }
    }

    public function index()
    {

        return view('administratif::user.index');
    }

    public function page_add() {

        $rs             = User::select('id', 'username', 'nama', 'alamat')
                            ->get();

        $rspermission   = DB::table('roles')->select('id', 'name', DB::raw("CONCAT(name) as display"))
                            ->where('name','!=','SUPERADMIN')
                            ->where('name','!=','PENGGUNA')
                            ->get();

        return view('administratif::user.add', ['rs' => $rs, 'rspermission' => $rspermission]);


    }

    public function store(Request $request) {

        $rspermission   = $request->rspermission;
        $check          = User::where('username', $request->username)->first();
        $check2         = User::onlyTrashed()->where('username', $request->username)->first();
        $usernames      = Auth::User()->username;

        if (!empty($check)) {
            return response()->json( [ 'status' => 'FAILED', 'message' => 'Duplicate' ] );
        }
        elseif (!empty($check2)) {
            return response()->json( [ 'status' => 'FAILED', 'message' => 'Trash' ] );
        } 

        $rs = User::firstOrCreate(
            ['username' => $request->username],
            ['nama' => $request->nama, 'password' => Hash::make($request->password), 'alamat' => $request->alamat, 'status' => 0]
        );
        

        $role = array([$rspermission]);

        $rs->assignRole($role);


        if ($rs){

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' membuat User baru dengan username '. $request->username);

            return response()->json(['status' => 'OK']);

        } else {

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' gagal membuat User baru dengan username '. $request->username);

            return response()->json(['status' => 'ERROR']);

        }
    }

    public function page_show($id) {

        $rs         = User::findOrfail($id);

        return view('administratif::user.show', ['rs' => $rs]);

    }

    public function edit(Request $request) {

        $result         = User::findOrfail($request->id);
        $username       = $request->username;
        $usernamelama   = $request->usernamelama;
        $rspermission   = $request->rspermission;
        $usernames      = Auth::User()->username;

        if ($usernamelama != $username) {
            $check      = User::where('username', $request->username)->first();
            $check2     = User::onlyTrashed()->where('username', $request->username)->first();

            if (!empty($check)) {
                return response()->json( [ 'status' => 'FAILED', 'message' => 'Duplicate' ] );

            }

            elseif (!empty($check2)) {
                return response()->json( [ 'status' => 'FAILED', 'message' => 'Trash' ] );
            }
        }

        $rs                       = User::find($result->id);
        $rs->username             = $request->username;
        $rs->nama                 = $request->nama;
        $rs->alamat               = $request->alamat;
        $rs->status               = $request->status;
        $rs->save();

        $role = array([$rspermission]);

        $rs->syncRoles($role);

        if ($rs){

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' mengubah detail username '. $request->username);

            return response()->json(['status' => 'OK']);

        } else {

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' gagal mengubah detail username '. $request->username);

            return response()->json(['status' => 'ERROR']);

        }
    }

    public function edit_password_profile(Request $request) {

        $result         = Auth::User()->id;
        $usernames      = Auth::User()->username;

        $rs             = User::find(Auth::User()->id);
        $rs->password   = Hash::make($request->password);
        $rs->save();


        if ($rs){

            activity()
            ->log($usernames. ' mengubah passwordnya');

            return response()->json(['status' => 'OK']);

        } else {

            activity()
            ->log($usernames. ' gagal mengubah passwordnya');

            return response()->json(['status' => 'ERROR']);

        }

    }

    public function edit_password(Request $request) {

        $result         = User::findOrfail($request->id);
        $rsnama         = $result->username;
        $usernames      = Auth::User()->username;

        $rs             = User::find($result->id);
        $rs->password   = Hash::make($request->password);
        $rs->save();


        if ($rs){

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' mengubah password username '. $rsnama);

            return response()->json(['status' => 'OK']);

        } else {

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' gagal mengubah password username '. $rsnama);

            return response()->json(['status' => 'ERROR']);

        }

    }

}