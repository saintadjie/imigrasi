<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;
use DB;
use Carbon\Carbon;
use Auth;

class MpasporGenerateController extends Controller
{
    protected $db;

    public function __construct() {
        $this->db = DB::connection('mpaspor');
    }

    public function pullData(Request $request) {

        $type       = $request->type;

        switch ($type) {
            case 'pullDataGenerate':
                try {
                    $date     = Carbon::now()->addDays(1)->startOfDay();

                    return datatables ($this->db->select("SELECT a.kode_permohonan, a.nik, a.nama ,b.tanggal_pengajuan, b.submit_date, a.status as status_detail, b.status as status_master, a.is_delete,a.id as id_detail, b.id as id_master from po_mpp.mpp_tbl_detail_booking_pemohon a join po_mpp.mpp_tbl_permohonan b on a.permohonan_id = b.id where a.status = -1 and a.is_delete in (0) and b.tanggal_pengajuan >= '$date' and b.status in (2,3,4) order by b.submit_date"))
                    ->addIndexColumn()
                    ->toJson();


                } catch (Exception $e) {
                    report($e);
                    abort(403, 'Unauthorized action.');
                    return false;
                }
            break;

            case 'pullDataGenerateYes':
                try {
                    $date     = Carbon::now()->addDays(1)->startOfDay();

                    return datatables ($this->db->select("SELECT a.kode_permohonan, a.nik, a.nama, b.tanggal_pengajuan, b.submit_date, a.status as status_detail, b.status as status_master, a.is_delete,a.id as id_detail, b.id as id_master from po_mpp.mpp_tbl_detail_booking_pemohon a join po_mpp.mpp_tbl_permohonan b on a.permohonan_id = b.id where a.status = -1 and a.is_delete in (0) and b.tanggal_pengajuan >= '$date' and b.status in (2,3,4)  and (a.kode_permohonan is not null AND a.kode_permohonan != '-') order by b.submit_date"))
                        ->addIndexColumn()
                        ->toJson();


                } catch (Exception $e) {
                    report($e);
                    abort(403, 'Unauthorized action.');
                    return false;
                }
            break;

            case 'pullDataGenerateNo':
                try {
                    $date     = Carbon::now()->addDays(1)->startOfDay();

                    return datatables ($this->db->select("SELECT a.kode_permohonan, a.nik, a.nama, b.tanggal_pengajuan, b.submit_date, a.status as status_detail, b.status as status_master, a.is_delete,a.id as id_detail, b.id as id_master from po_mpp.mpp_tbl_detail_booking_pemohon a join po_mpp.mpp_tbl_permohonan b on a.permohonan_id = b.id where a.status = -1 and a.is_delete in (0) and b.tanggal_pengajuan >= '$date' and b.status in (2,3,4)  and (a.kode_permohonan is null OR a.kode_permohonan = '-') order by b.submit_date"))
                        ->addIndexColumn()
                        ->toJson();


                } catch (Exception $e) {
                    report($e);
                    abort(403, 'Unauthorized action.');
                    return false;
                }
            break;

            case 'pullDataGenerateKadaluarsa':
                try {
                    $date     = Carbon::now()->endOfDay();

                    return datatables ($this->db->select("SELECT a.kode_permohonan, a.nik, a.nama, b.tanggal_pengajuan, b.submit_date, a.status as status_detail, b.status as status_master, a.is_delete,a.id as id_detail, b.id as id_master from po_mpp.mpp_tbl_detail_booking_pemohon a join po_mpp.mpp_tbl_permohonan b on a.permohonan_id = b.id where a.status = -1 and a.is_delete in (0) and b.tanggal_pengajuan <= '$date' and b.status in (2,3,4) order by b.submit_date"))
                        ->addIndexColumn()
                        ->toJson();


                } catch (Exception $e) {
                    report($e);
                    abort(403, 'Unauthorized action.');
                    return false;
                }
            break;
        
        }

        

    }

    public function generate_yes(Request $request)
    {

        $id_permohonan_yes  = $request->id_permohonan_yes;
        $usernames          = Auth::User()->username;
        
        $update_master  = $this->db->update(DB::raw("UPDATE po_mpp.mpp_tbl_permohonan SET status = '4' WHERE id IN ($id_permohonan_yes)"));

        if ($update_master){

            $usernames          = Auth::User()->username;
            $url                = "http://10.20.66.13:8089/api/simponi/bridge/trigger-generate-billing";
            $rstoken            = $this->db->select(DB::raw("SELECT a.uuid_token FROM po_uma.uma_jwt_token a join po_uma.uma_tbl_users b on a.user_id = b.id WHERE a.is_expired = '0' and b.role_id = '2' ORDER BY a.expired_at desc LIMIT 1"));

            $curl   = curl_init($url);
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            $headers = array(
               "Accept: application/json",
               "Authorization: Bearer ". $rstoken[0]->uuid_token,
               "Content-Type: application/json",
            );
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

            $hasil = curl_exec($curl);
            curl_close($curl);
            $rs = json_decode($hasil);

            if ($rs->status == 'true'){
                
                activity()
                ->withProperties(['username' => $usernames])
                ->log($usernames. ' berhasil merefresh Kode Billing dengan ID master permohonan '. $id_permohonan_yes);

                return response()->json(['status' => 'OK']);

            } else {
                
                activity()
                ->withProperties(['username' => $usernames])
                ->log($usernames. ' gagal merefresh Kode Billing dengan ID master permohonan '. $id_permohonan_yes);

                return response()->json(['status' => 'ERROR']);
            }

        } else {

            activity()
                ->withProperties(['username' => $usernames])
                ->log($usernames. ' gagal merefresh Kode Billing dengan ID master permohonan '. $id_permohonan_yes);

            return response()->json(['status' => 'ERROR']);
        }

    }

    public function generate_no(Request $request)
    {

        $id_permohonan_no   = $request->id_permohonan_no;
        $usernames          = Auth::User()->username;
        $url                = "http://10.20.66.5:8080/validator/application-code";

        $curl   = curl_init($url);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); 
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $headers = array(
           "Accept: application/json",
           "Content-Type: application/json",
        );
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $data = '{"passport_application_id_list": [
                    '.$id_permohonan_no.'
                ]}';

        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

        $hasil = curl_exec($curl);
        curl_close($curl);
        $rs = json_decode($hasil);

        if ($rs){

            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' berhasil merefresh Kode Billing dan Kode Permohonan dengan detail ID permohonan '. $id_permohonan_no);

            return response()->json(['status' => 'OK']);

        } else {
            
            activity()
            ->withProperties(['username' => $usernames])
            ->log($usernames. ' gagal merefresh Kode Billing dan Kode Permohonan dengan detail ID permohonan '. $id_permohonan_no);

            return response()->json(['status' => 'ERROR']);
        }

    }

    public function generate_kadaluarsa(Request $request)
    {

        $usernames      = Auth::User()->username;
        
        $date     = Carbon::now()->endOfDay();
        $rs = ($this->db->select("SELECT a.kode_permohonan, a.nik, a.nama, b.tanggal_pengajuan, b.submit_date, a.status as status_detail, b.status as status_master, a.is_delete,a.id as id_detail, b.id as id_master from po_mpp.mpp_tbl_detail_booking_pemohon a join po_mpp.mpp_tbl_permohonan b on a.permohonan_id = b.id where a.status = -1 and a.is_delete in (0) and b.tanggal_pengajuan <= '$date' and b.status in (2,3,4) order by b.submit_date"));

        $rs_id_details  = [];
        $rs_id_masters  = [];

        foreach ($rs as $rs) {
            $rs_id_details[] =  $rs->id_detail;
            $rs_id_masters[] =  $rs->id_master;
        }
        $json_detail    = json_encode($rs_id_details) ;
        $json_master    = json_encode($rs_id_masters) ;

        $search         = array('[', ']');
        $replace        = array('', '');
        $result_detail  = str_replace($search, $replace, $json_detail);
        $result_master  = str_replace($search, $replace, $json_master);


        $update_detail  = $this->db->update(DB::raw("UPDATE po_mpp.mpp_tbl_detail_booking_pemohon SET status = '4' WHERE id IN ($result_detail)"));

        if ($update_detail){

            $update_master  = $this->db->update(DB::raw("UPDATE po_mpp.mpp_tbl_permohonan SET status = '4' WHERE id IN ($result_master)"));

            if ($update_master){
                
                activity()
                ->withProperties(['username' => $usernames])
                ->log($usernames. ' berhasil merubah kadaluarsa detail dan master permohonan');

                return response()->json(['status' => 'OK']);

            } else {
                
                activity()
                ->withProperties(['username' => $usernames])
                ->log($usernames. ' gagal merubah kadaluarsa master permohonan');

                return response()->json(['status' => 'ERROR']);
            }

        } else {

            activity()
                ->withProperties(['username' => $usernames])
                ->log($usernames. ' gagal merubah kadaluarsa detail dan master permohonan');

            return response()->json(['status' => 'ERROR']);
        }

    }


    public function index()
    {
        
        $date     = Carbon::now()->format('d-m-Y');

        return view('administratif::mpaspor.generate',['date' => $date]);
    }
}
