<?php

namespace App\Modules\Administratif\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\ActivityLog;

class LogsController extends Controller
{
    public function pullData(Request $request) {

        try {
            return datatables (ActivityLog::select('description', 'properties', 'created_at')
                ->latest())
                ->addIndexColumn()
                ->toJson();

            } catch (Exception $e) {
            report($e);
            abort(403, 'Unauthorized action.');
            return false;
        }
    }

    public function index()
    {

        return view('administratif::logs.index');
    }
}
