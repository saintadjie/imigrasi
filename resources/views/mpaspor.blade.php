@extends('layouts.outer')
@push('script-header')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <style type="text/css">
        .titik {
          width: 100%;
          text-overflow: ellipsis;
          overflow: hidden;
        }
        .swal-wide{
            width:100% !important;
        }

        .sweet_loader {
            width: 140px;
            height: 140px;
            margin: 0 auto;
            animation-duration: 0.5s;
            animation-timing-function: linear;
            animation-iteration-count: infinite;
            animation-name: ro;
            transform-origin: 50% 50%;
            transform: rotate(0) translate(0,0);
        }
        @keyframes ro {
            100% {
                transform: rotate(-360deg) translate(0,0);
            }
        }
    </style>
@endpush

@section('content')

<div class="section-body">
    <h2 class="section-title">Tabs</h2>
    <p class="section-lead">The tab component for dividing parts of content.</p>

    <div class="row">
              
        <div class="col-12 col-sm-12 col-lg-12">
        	<div class="card">
            	<div class="card-header">
                    <h4>Bordered Tab</h4>
            	</div>
            	<div class="card-body">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                      	<li class="nav-item">
                        	<a class="nav-link active" id="check-tab" data-toggle="tab" href="#check2" role="tab" aria-controls="check" aria-selected="true">Check</a>
                      	</li>
                      	<li class="nav-item">
                        	<a class="nav-link" id="simponi-tab" data-toggle="tab" href="#simponi" role="tab" aria-controls="simponi" aria-selected="false">Simponi</a>
                      	</li>
                      	<li class="nav-item">
                        	<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact2" role="tab" aria-controls="contact" aria-selected="false">Belum Kepikiran</a>
                      	</li>
                    </ul>
                    <div class="tab-content tab-bordered" id="myTab3Content">
                      	<div class="tab-pane fade show active" id="check2" role="tabpanel" aria-labelledby="check-tab">
                        	<div class="card">
                                <div class="card-body">
                                    <div class="list-group">
                                        <a href="#" class="list-group-item list-group-item-action flex-column align-items-start list-group-item-dark">
                                            <div class="d-flex w-100 justify-content-between">
                                                <h5 class="mb-1">Check Kode Permohonan</h5>
                                            </div>
                                            <p class="mb-1">Masukkan Kode Permohonan, NIK atau Kode Billing Permohonan yang akan dicek</p>
                                        </a>
                                        <a class="list-group-item list-group-item-action flex-column align-items-start">
                                            <div class="w-100 justify-content-between">
                                                <div class="row">
                                                    <div class="col-12 col-md-12 col-lg-12">
                                                        <div class="form-group">
                                                            <label>Kode Permohonan, NIK atau Kode Billing</label>
                                                            <input type="number" id="kpnkb" class="form-control" aria-describedby="kpnkbHelpBlock">
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12 col-md-12 col-lg-12">
                                                    <button id="btn_kpnkb" class="btn btn-dark btn-rounded btn-lg waves-effect waves-light" style="float: right;">Check</button>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                      	</div>
                      	<div class="tab-pane fade" id="simponi" role="tabpanel" aria-labelledby="simponi-tab">
                        	<div class="card">
                                <div class="card-body">
                                    <div class="list-group">
                                        <a href="#" class="list-group-item list-group-item-action flex-column align-items-start list-group-item-dark">
                                            <div class="d-flex w-100 justify-content-between">
                                                <h5 class="mb-1">Check Pembayaran Simponi</h5>
                                            </div>
                                            <p class="mb-1">Masukkan Kode Billing Permohonan yang akan dicek</p>
                                        </a>
                                        <a class="list-group-item list-group-item-action flex-column align-items-start">
                                            <div class="w-100 justify-content-between">
                                                <div class="row">
                                                    <div class="col-12 col-md-12 col-lg-12">
                                                        <div class="form-group">
                                                            <label>Kode Billing</label>
                                                            <input type="number" id="kode_billing" class="form-control" aria-describedby="kode_billingHelpBlock">
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-12 col-md-12 col-lg-12">
                                                    <button id="btn_kode_billing" class="btn btn-dark btn-rounded btn-lg waves-effect waves-light" style="float: right;">Check</button>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                      	</div>
                      	<div class="tab-pane fade" id="contact2" role="tabpanel" aria-labelledby="contact-tab">
                        	Vestibulum imperdiet odio sed neque ultricies, ut dapibus mi maximus. Proin ligula massa, gravida in lacinia efficitur, hendrerit eget mauris. Pellentesque fermentum, sem interdum molestie finibus, nulla diam varius leo, nec varius lectus elit id dolor. Nam malesuada orci non ornare vulputate. Ut ut sollicitudin magna. Vestibulum eget ligula ut ipsum venenatis ultrices. Proin bibendum bibendum augue ut luctus.
                      	</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@push('script-footer')
    <script src="{{url('js/public/mpaspor/index_app.js')}}"></script>
 
    <script type="text/javascript">
        var url_main                    = "{{url('mpaspor/')}}"
        var url_api_kpnkb               = "{{url('api/v1/public/mpaspor/get_permohonanPm')}}"
        var url_api_simponi             = "{{url('api/v1/public/mpaspor/check_simponiPm')}}"
        var sweet_loader = '<div class="sweet_loader"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="margin: auto; background: none; display: block; shape-rendering: auto;" width="140px" height="140px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid"><circle cx="50" cy="50" r="0" fill="none" stroke="#014c86" stroke-width="2"><animate attributeName="r" repeatCount="indefinite" dur="1s" values="0;40" keyTimes="0;1" keySplines="0 0.2 0.8 1" calcMode="spline" begin="0s"></animate><animate attributeName="opacity" repeatCount="indefinite" dur="1s" values="1;0" keyTimes="0;1" keySplines="0.2 0 0.8 1" calcMode="spline" begin="0s"></animate></circle><circle cx="50" cy="50" r="0" fill="none" stroke="#00a950" stroke-width="2"><animate attributeName="r" repeatCount="indefinite" dur="1s" values="0;40" keyTimes="0;1" keySplines="0 0.2 0.8 1" calcMode="spline" begin="-0.5s"></animate><animate attributeName="opacity" repeatCount="indefinite" dur="1s" values="1;0" keyTimes="0;1" keySplines="0.2 0 0.8 1" calcMode="spline" begin="-0.5s"></animate></circle></svg></div>';
    </script>


@endpush
@endsection
