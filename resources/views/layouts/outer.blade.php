
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>IMIGRASI</title>
    <link rel="icon" type="image/png" href="{{url('passport.png')}}"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- General CSS Files -->
    @stack('script-header')
    <!-- General CSS Files -->
    <link rel="stylesheet" type="text/css" href="{{url('out/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('out/css/all.min.css')}}">

    <!-- Template CSS -->
    <link rel="stylesheet" type="text/css" href="{{url('out/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('out/css/components.css')}}">
    <link rel="stylesheet" type="text/css" href="{{url('out/css/sweetalert2/sweetalert2.min.css')}}" />
    <style type="text/css">
        
    </style>
</head>

<body class="layout-3">
    <div id="app">
    <div class="main-wrapper container">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <a href="{{url('/')}}" class="navbar-brand sidebar-gone-hide">IMIGRASI</a>
        <a href="#" class="nav-link sidebar-gone-show" data-toggle="sidebar"><i class="fas fa-bars"></i></a>
        <div class="nav-collapse">
          <a class="sidebar-gone-show nav-collapse-toggle nav-link" href="#">
            <i class="fas fa-ellipsis-v"></i>
          </a>
          <ul class="navbar-nav">
            <li class="nav-item active"><a href="{{url('mpaspor')}}" class="nav-link">M-Paspor</a></li>
            <li class="nav-item"><a href="#" class="nav-link">Bla-Bla-Bla</a></li>
            <li class="nav-item"><a href="#" class="nav-link">Lainnya</a></li>
          </ul>
        </div>
        
        
      </nav>

      <nav class="navbar navbar-secondary navbar-expand-lg">
        <div class="container">
          <ul class="navbar-nav">
            <li class="nav-item dropdown">
              <a href="#" data-toggle="dropdown" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
              <ul class="dropdown-menu">
                <li class="nav-item"><a href="dashboard-general.html" class="nav-link">General Dashboard</a></li>
                <li class="nav-item"><a href="dashboard-ecommerce.html" class="nav-link">Ecommerce Dashboard</a></li>
              </ul>
            </li>
            <li class="nav-item active">
              <a href="#" class="nav-link"><i class="far fa-heart"></i><span>Top Navigation</span></a>
            </li>
            <li class="nav-item dropdown">
              <a href="#" data-toggle="dropdown" class="nav-link has-dropdown"><i class="far fa-clone"></i><span>Multiple Dropdown</span></a>
              <ul class="dropdown-menu">
                <li class="nav-item"><a href="#" class="nav-link">Not Dropdown Link</a></li>
                <li class="nav-item dropdown"><a href="#" class="nav-link has-dropdown">Hover Me</a>
                  <ul class="dropdown-menu">
                    <li class="nav-item"><a href="#" class="nav-link">Link</a></li>
                    <li class="nav-item dropdown"><a href="#" class="nav-link has-dropdown">Link 2</a>
                      <ul class="dropdown-menu">
                        <li class="nav-item"><a href="#" class="nav-link">Link</a></li>
                        <li class="nav-item"><a href="#" class="nav-link">Link</a></li>
                        <li class="nav-item"><a href="#" class="nav-link">Link</a></li>
                      </ul>
                    </li>
                    <li class="nav-item"><a href="#" class="nav-link">Link 3</a></li>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>

      <!-- Main Content -->
      <div class="main-content">
        <section class="section">
        @yield('content')
        </section>
      </div>
      <footer class="main-footer">
        <div class="footer-left">
          Copyright &copy; 2022 <div class="bullet"></div> Design For <a href="#">IMIGRASI</a>
        </div>
        <div class="footer-right">
          
        </div>
      </footer>
    </div>
  </div>

    <!-- General JS Scripts -->
    <script src="{{url('out/js/jquery.min.js')}}"></script>
    <script src="{{url('out/js/popper.js')}}"></script>
    <script src="{{url('out/js/tooltip.js')}}"></script>
    <script src="{{url('out/js/bootstrap.min.js')}}"></script>
    <script src="{{url('out/js/jquery.nicescroll.min.js')}}"></script>
    <script src="{{url('out/js/moment.min.js')}}"></script>
    <script src="{{url('out/js/stisla.js')}}"></script>



    <!-- Template JS File -->
    <script src="{{url('out/js/scripts.js')}}"></script>
    <script src="{{url('out/js/custom.js')}}"></script>
    <script src="{{url('out/css/sweetalert2/sweetalert2.min.js')}}"></script>


    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                'X-Requested-With': 'XMLHttpRequest',
            }
        })
    </script>
    <script type="text/javascript">
        var backtotop = $('#backtotop');

        $(window).scroll(function() {
          if ($(window).scrollTop() > 300) {
            backtotop.addClass('show');
          } else {
            backtotop.removeClass('show');
          }
        });

        backtotop.on('click', function(e) {
          e.preventDefault();
          $('html, body').animate({scrollTop:0}, '300');
        });


    </script>
    @stack('script-footer')
</body>
</html>